import { ChatRoomDocument } from '@app/models/odms/chatRoom/schema'
import { ChatService } from '@app/services/chat'
import { newChatRoomForUserEvent, NotifierService } from '@app/services/notifier'
import Joi from 'joi'
import { Types } from 'mongoose'
import { Server, Socket } from 'socket.io'

export enum Events {
    ECHO = 'echo', // reply with the same message
    MESSAGE = 'message', // send message to a specified room
    READY = 'ready', // server structures are prepared, you can send your events
    SYNC = 'sync', // synchronize rooms and messages
    LIVE = 'live', // you're live!
    NEW_ROOM = 'new_room', // notify new room creation
}

export interface ErrorMessage {
    error: boolean
    message: string
}

export interface SocketMessage {
    roomId: string
    content: string
}

const SocketMessageValidator = Joi.object({
    roomId: Joi.string().required(),
    content: Joi.string().required(),
}).required()

export default async function handler(
    socket: Socket,
    chatService: ChatService,
    serverSocket: Server,
    notifierService: NotifierService
): Promise<void> {
    const sockAuth = socket as SocketAuthenticated
    const user = sockAuth.user
    const rooms = await chatService.roomsByLastUpdated(user._id)
    const roomIds = new Set()
    let synchronized = false
    let live = false

    const newRoomEventId = newChatRoomForUserEvent(user._id)
    const listener = notifierService.watchEvent(newRoomEventId, async (room) => {
        socket.emit(Events.NEW_ROOM, room as ChatRoomDocument)
        socket.join((room as ChatRoomDocument).id)
        roomIds.add((room as ChatRoomDocument).id)
    })

    socket.on('disconnect', () => {
        notifierService.unwatchEvent(newRoomEventId, listener)
    })

    socket.on(Events.ECHO, (msg) => {
        socket.emit(Events.ECHO, msg)
    })

    socket.on(Events.SYNC, async () => {
        if (synchronized) {
            socket.emit(Events.SYNC, <ErrorMessage>{
                error: true,
                message: 'Already synchronized',
            })
            return
        }

        synchronized = true
        socket.emit(Events.SYNC, rooms)

        for (const room of rooms) {
            await socket.join(room.id)
            roomIds.add(room.id)
        }

        live = true
        socket.emit(Events.LIVE)
    })

    socket.on(Events.MESSAGE, async (msg: SocketMessage) => {
        if (!live) {
            socket.emit(Events.MESSAGE, <ErrorMessage>{
                error: true,
                message: 'You are not live yet',
            })
            return
        }

        try {
            await SocketMessageValidator.validateAsync(msg)
        } catch (e) {
            socket.emit(Events.MESSAGE, <ErrorMessage>{
                error: true,
                message: 'Invalid message format',
            })
            return
        }

        if (roomIds.has(msg.roomId) === false) return
        const savedMessage = await chatService.saveMessage(user._id, Types.ObjectId(msg.roomId), msg.content)
        serverSocket.in(msg.roomId).emit(Events.MESSAGE, savedMessage)
    })

    socket.emit(Events.READY)
}
