import { Server as HttpServer } from 'http'
import { Server as SocketServer, Socket } from 'socket.io'
import { jwtSocketMiddleware } from './auth/jwt'
import { ChatService } from './services/chat'
import { NotifierService } from './services/notifier'
import socketHandler from './sockets'

export default function buildSocketServer(
    httpServer: HttpServer,
    notifierService: NotifierService,
    chatService: ChatService
): SocketServer {
    const socketServer = new SocketServer(httpServer)
    socketServer.use(jwtSocketMiddleware)
    socketServer.on('connection', (socket: Socket) => socketHandler(socket, chatService, socketServer, notifierService))
    return socketServer
}
