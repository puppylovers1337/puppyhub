import { connect, connection } from 'mongoose'
import { UserODM } from '@models/odms/user'
import { ChatService } from '@services/chat'
import { ChatRoomModel } from './models/odms/chatRoom/schema'
import { DogODM } from './models/odms/dog'
import { fakeDog, fakeUser } from './utils/faker'
import { Sex } from './models/interfaces/dog'
import { MatchModel } from './models/odms/match'
import { MessageModel } from './models/odms/message'
import { MarketPostODM } from './models/odms/marketPost'
import { BREEDS } from '@data/breeds'
import { MarketPostType } from './models/interfaces/marketPost'

const dbConfig = {
    host: process.env.DB_HOST,
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    pass: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
}
const dbUri = `mongodb://${dbConfig.user}:${dbConfig.pass}@${dbConfig.host}:${dbConfig.port}/${dbConfig.name}?authSource=admin`

export function setupDatabase(
    MatchODM: MatchModel,
    MessageODM: MessageModel,
    ChatRoomODM: ChatRoomModel,
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    chatService: ChatService
): void {
    connect(dbUri, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    connection.on('error', console.error.bind(console, 'MongoDB connection error:'))
    connection.on('open', async () => {
        if (process.env.NODE_ENV == 'development') {
            await MatchODM.remove({})
            await MessageODM.remove({})
            await UserODM.remove({})
            await ChatRoomODM.remove({})
            await DogODM.remove({})
            await MarketPostODM.remove({})

            console.log('Creating fake data...')

            const adrian = await fakeUser('Adrian', 'user0.jpeg')
            const elena = await fakeUser('Elena', 'user1.jpg')

            await fakeDog(
                adrian,
                { type: 'Point', coordinates: [0, 0] },
                Sex.MALE,
                BREEDS[17],
                'Jake',
                `
Sneaker chewer and bacon addict. Really into fitness. Work hard, bark hard. 
I stil like going hard with my boys at the park sometimes, but I'm looking to settle down with a cutie.
Unde esti tu, Tepes Doamne, sa-i imparti in doua cete
            `,
                'jake.jpg'
            )

            await fakeDog(
                adrian,
                { type: 'Point', coordinates: [0, 0] },
                Sex.FEMALE,
                BREEDS[23],
                'Mia',
                `
Gemini. Total foodie looking for a special walking partner. 
Preferably with pedigree. If my owner doesn't like you, neither do I! 
If you don't go to the groomer every week, dm me ;)
?- is_dead(god)
            `,
                'mia.jpg'
            )

            await fakeDog(
                adrian,
                { type: 'Point', coordinates: [0, 0] },
                Sex.MALE,
                BREEDS[12],
                'Aki',
                `
I have been sniffing people together with my hooman for more than 3 years. 
I like beating about the bush, am friendly with Chihuahuas and love playing with children’s toys in the sandbox! 
If you wanna ‘bark on an adventure with me, woof me here!
La Ploiesti poti sa te pitesti, dar la Pitesti poti sa te ploiesti?
            `,
                'aki.jpg'
            )

            await fakeDog(
                elena,
                { type: 'Point', coordinates: [0, 0] },
                Sex.MALE,
                BREEDS[9],
                'Luigi',
                `
I definetly have the Italian spice, so if you wanna bark to Positano with me while drinking some Prosecco and savouring some pasta con pesto con Pedigree meat balls, dm me! 
I live for the luxurious life so if you wanna wag your tail in the riches of life, show me your Pedigree medals here!
Black holes emit Hawking radiation.
            `,
                'luigi.jpg'
            )

            await fakeDog(
                elena,
                { type: 'Point', coordinates: [0, 0] },
                Sex.FEMALE,
                BREEDS[17],
                'Lucy',
                `
I am a cute ball-catching superstar who loves sports and spending time with friends. 
A perfect day for me includes going for a jog or playing ball, stopping by a training class, then snuggling up for a nap.
#define true (rand() > 500)
            `,
                'lucy.jpg'
            )

            await fakeDog(
                elena,
                { type: 'Point', coordinates: [0, 0] },
                Sex.MALE,
                BREEDS[23],
                'Theo',
                `
Hi! I am Theo and my hooman says I am Teodorable! 
I enjoy nice walks in the park, princessing around and going to fashion contests with my besties! 
If you also have 2 best friends, dm me and we could go on a wiggly time travel!
Doggos go to the moon!
            `,
                'theo.jpg'
            )

            await MarketPostODM.create({
                user_id: adrian._id,
                title: 'Tom',
                description: `
Hi, my name is Tom and I am looking for a nice hooman that can pat me at any hour in the day and take care of my wiggly tail! 
If you think you match the description, then pm asap! Rohan Strauss loves dark beer.
`,
                price: '1000$',
                type: MarketPostType.SELL,
                location: 'Vaslui',
                email: 'adrian@yahoo.com',
                phoneNumber: '+40 70 noi doi si-ale',
                images: ['aki.jpg'],
            })

            await MarketPostODM.create({
                user_id: elena._id,
                title: 'Lolita',
                description: `
Hi there, hooman! I am looking for a doggo mommy who can love and take care for me unconditionally!
I dream of ballet contests and nice outfits to wear in the sun! 
If you want to play with me all day long, then it’s a match!
And so on and so on.
`,
                price: '1500$',
                type: MarketPostType.SELL,
                location: 'Vaslui',
                email: 'cornelia@yahoo.com',
                phoneNumber: '+40 722 222 222',
                images: ['mia.jpg'],
            })

            await MarketPostODM.create({
                user_id: elena._id,
                title: 'Elena is babysitting',
                description: `
Paws up! I am looking for a nice hooman to play with me while my mommy is at work!!!!! 
I am very fussy about my food, chase pigeons in the park and also love biting kiddos in the playground! 
If you think you can rise up to this challenge, then I am definetly looking forward to meeting you!
Existence precedes essence.
`,
                price: '15$ / hr',
                type: MarketPostType.BABYSIT,
                location: 'Vaslui',
                email: 'elena@yahoo.com',
                phoneNumber: '+40 tale buze moi',
                images: ['user1.jpg'],
            })
        }

        console.log('Database setup done.')
    })
}
