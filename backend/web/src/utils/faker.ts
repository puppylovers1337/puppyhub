import { Sex } from '@app/models/interfaces/dog'
import { PointInterface } from '@app/models/interfaces/location'
import { DogDocument, DogODM } from '@app/models/odms/dog'
import { UserDocument, UserODM } from '@app/models/odms/user'
import { BREEDS } from 'data/breeds'

let counter = 0
const defaultPassword = '1234'

export function fakeUser(name?: string, image?: string): Promise<UserDocument> {
    const email = name !== undefined ? `${name}@puppyhub.com` : undefined
    return UserODM.create({
        username: name || `USERUM${counter++}`,
        password: defaultPassword,
        email: email || `userum${counter - 1}@puppyhub.com`,
        ...(image !== undefined && { profile: { image } }),
    })
}

export function fakeUsers(n: number): Promise<UserDocument[]> {
    return Promise.all(
        Array(n)
            .fill(null)
            .map(() => fakeUser())
    )
}

export function fakeDog(
    user: UserDocument,
    location: PointInterface,
    sex: Sex,
    breed?: string,
    name?: string,
    description?: string,
    image?: string
): Promise<DogDocument> {
    return DogODM.create({
        user_id: user._id,
        name: name || `DOGGUS${counter++}`,
        breed: breed || BREEDS[0],
        sex,
        location,
        ...(description !== undefined && { description }),
        ...(image !== undefined && { images: [image] }),
    })
}
