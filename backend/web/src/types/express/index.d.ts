import { Socket } from 'socket.io'
import { UserDocument } from 'src/models/odms/user'

declare global {
    namespace Express {
        // eslint-disable-next-line @typescript-eslint/no-empty-interface
        interface User extends UserDocument {}
    }

    interface SocketExtra extends Socket {
        user?: Express.User
    }

    interface SocketAuthenticated extends Socket {
        user: Express.User
    }
}

export {}
