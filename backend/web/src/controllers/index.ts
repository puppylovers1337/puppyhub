import { Router } from 'express'
import 'express-async-errors'

import { router as authRouter } from './auth'
import { router as usersRouter } from './users'
import { router as dogsRouter } from './dogs'
import { router as marketPostRouter } from './marketPost'
import { buildRouter as buildMatchRouter } from './match'
import { buildRouter as buildChatRouter } from './chat'
import { ChatService } from '@app/services/chat'
import { MatchService } from '@app/services/match'

export default function buildRouter(chatService: ChatService, matchService: MatchService): Router {
    const router = Router()
    router.use('/auth', authRouter)
    router.use('/users', usersRouter)
    router.use('/dogs', dogsRouter)
    router.use('/match', buildMatchRouter(matchService))
    router.use('/chat', buildChatRouter(chatService))
    router.use('/marketposts', marketPostRouter)

    return router
}
