import { Router } from 'express'
import 'express-async-errors'

import {
    MarketPostUpdateDTO,
    MarketPostUpdateSchema,
    MarketPostParamsSchema,
    MarketPostParamsDTO,
    MarketPostCreateSchema,
    MarketPostCreateDTO,
    MarketPostImageUploadSchema,
    MarketPostImageUploadDTO,
    MarketPostImageDeleteSchema,
    MarketPostImageDeleteDTO,
} from '@app/models/dtos/marketPost'
import { MarketPostODM } from '@app/models/odms/marketPost'
import { checkContract, notNullContract } from 'src/utils/contracts'
import { authenticated } from 'src/auth'
import { ControllerError } from './error'
import validator from 'src/models/dtos/validator'
import { ValidatedRequest } from 'express-joi-validation'
import { StatusCodes } from 'http-status-codes'
import { fileStorage } from '@app/storage'
import { rename } from 'fs/promises'
import mimedb from 'mime-db'
import { deleteIfFails } from '@app/utils/fs'

export const router = Router()

router.get('/', authenticated(), async (req, res) => {
    const currentUser = notNullContract(req.user, 'Null user')

    const posts = await MarketPostODM.find({
        user_id: {
            $nin: [currentUser._id],
        },
    }).exec()

    res.json(posts)
})

router.get('/me', authenticated(), async (req, res) => {
    const currentUser = notNullContract(req.user, 'Null user')

    const posts = await MarketPostODM.find({
        user_id: currentUser._id,
    }).exec()

    res.json(posts)
})

router.get(
    '/:id',
    validator.params(MarketPostParamsSchema),
    async (req: ValidatedRequest<MarketPostParamsDTO>, res) => {
        const post = notNullContract(
            await MarketPostODM.findById(req.params.id).exec(),
            new ControllerError('Market post not found!', StatusCodes.NOT_FOUND)
        )
        res.json(post)
    }
)

router.put(
    '/:id/images',
    authenticated(),
    fileStorage.single('image'),
    validator.params(MarketPostParamsSchema),
    async (req: ValidatedRequest<MarketPostImageUploadDTO>, res) => {
        await MarketPostImageUploadSchema.validateAsync(req.file)

        await deleteIfFails(async () => {
            const currentUser = notNullContract(req.user, 'Null user')
            const post = notNullContract(
                await MarketPostODM.findById(req.params.id).exec(),
                new ControllerError('Post not found', StatusCodes.NOT_FOUND)
            )

            console.log(post.user_id, currentUser._id)
            checkContract(
                post.user_id.equals(currentUser._id),
                new ControllerError("You cannot upload images for a post you don't own", StatusCodes.FORBIDDEN)
            )

            const extensions = notNullContract(
                mimedb[req.file.mimetype].extensions,
                new ControllerError('Unknown MIME type', StatusCodes.BAD_REQUEST)
            )

            const filenameWithExtension = `${req.file.filename}.${extensions[0]}`

            await rename(req.file.path, `${req.file.path}.${extensions[0]}`)
            await post.updateOne({
                $push: {
                    images: filenameWithExtension,
                },
            })

            res.json({ filenameWithExtension })
        }, req.file)
    }
)

router.post(
    '/',
    authenticated(),
    validator.body(MarketPostCreateSchema),
    async (req: ValidatedRequest<MarketPostCreateDTO>, res) => {
        const post = await MarketPostODM.create({
            user_id: notNullContract(req.user, 'Null user').id,
            ...req.body,
        })

        res.json(post)
    }
)

router.put(
    '/:id',
    authenticated(),
    validator.params(MarketPostParamsSchema),
    validator.body(MarketPostUpdateSchema),
    async (req: ValidatedRequest<MarketPostUpdateDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const post = notNullContract(
            await MarketPostODM.findById(req.params.id).exec(),
            new ControllerError('Market post not found!', StatusCodes.NOT_FOUND)
        )

        checkContract(
            post.user_id.equals(currentUser._id),
            new ControllerError('You cannot modify a post you do not own', StatusCodes.FORBIDDEN)
        )

        const updated = await post.updateOne(req.body).exec()
        res.json(updated)
    }
)

router.delete(
    '/:id',
    authenticated(),
    validator.params(MarketPostParamsSchema),
    async (req: ValidatedRequest<MarketPostParamsDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const post = notNullContract(
            await MarketPostODM.findById(req.params.id).exec(),
            new ControllerError('Market post not found!', StatusCodes.NOT_FOUND)
        )

        checkContract(
            post.user_id.equals(currentUser._id),
            new ControllerError('You cannot delete a post you do not own', StatusCodes.FORBIDDEN)
        )

        await MarketPostODM.findOneAndDelete({ _id: req.params.id }).exec()
        res.json({ message: 'Post deleted!' })
    }
)

router.delete(
    '/:id/images/:imgName',
    authenticated(),
    validator.params(MarketPostImageDeleteSchema),
    async (req: ValidatedRequest<MarketPostImageDeleteDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const post = notNullContract(
            await MarketPostODM.findById(req.params.id).exec(),
            new ControllerError('Market post not found!', StatusCodes.NOT_FOUND)
        )

        checkContract(
            post.user_id.equals(currentUser._id),
            new ControllerError('You cannot delete an image for a post you do not own', StatusCodes.FORBIDDEN)
        )

        checkContract(
            post.images.includes(req.params.imgName),
            new ControllerError('Image not found', StatusCodes.NOT_FOUND)
        )

        await post.updateOne({
            $pull: {
                images: req.params.imgName,
            },
        })

        // TODO: actually delete the file

        res.json({ message: 'deleted' })
    }
)
