import { Router } from 'express'
import 'express-async-errors'

import { authenticated } from '@app/auth'
import { MatchCreateDTO, MatchCreateSchema, MatchQueryDTO, MatchQuerySchema } from '@app/models/dtos/match'
import validator from '@app/models/dtos/validator'
import { MatchService } from '@app/services/match'
import { notNullContract } from '@app/utils/contracts'
import { ValidatedRequest } from 'express-joi-validation'

export function buildRouter(matchService: MatchService): Router {
    const router = Router()

    router.get(
        '/',
        authenticated(),
        validator.query(MatchQuerySchema),
        async (req: ValidatedRequest<MatchQueryDTO>, res) => {
            const currentUser = notNullContract(req.user, 'User is null')
            const dogs = await matchService.findDogsAround(req.query, currentUser)
            res.json(dogs)
        }
    )

    router.post(
        '/',
        authenticated(),
        validator.body(MatchCreateSchema),
        async (req: ValidatedRequest<MatchCreateDTO>, res) => {
            const currentUser = notNullContract(req.user, 'User is null')
            const result = matchService.matchDogs(currentUser, req.body.sourceDogId, req.body.targetDogId)
            res.json({ status: result })
        }
    )

    return router
}
