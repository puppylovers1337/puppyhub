import { Router } from 'express'
import 'express-async-errors'

import { authenticated } from 'src/auth'
import {
    DogParamsSchema,
    DogParamsDTO,
    DogUpdateSchema,
    DogUpdateDTO,
    DogCreateSchema,
    DogCreateDTO,
    DogImageDeleteSchema,
    DogImageDeleteDTO,
    DogImageUploadDTO,
    DogImageUploadSchema,
} from 'src/models/dtos/dog'
import validator from 'src/models/dtos/validator'
import { DogODM } from 'src/models/odms/dog'
import { checkContract, notNullContract } from 'src/utils/contracts'
import { ValidatedRequest } from 'express-joi-validation'
import { StatusCodes } from 'http-status-codes'
import { ControllerError } from './error'
import { fileStorage } from '@app/storage'
import { rename } from 'fs/promises'
import mimedb from 'mime-db'
import { deleteIfFails } from '@app/utils/fs'
import { BREEDS } from 'data/breeds'

export const router = Router()

router.get('/breeds', authenticated(), async (req, res) => {
    res.json(BREEDS)
})

router.get('/:id', validator.params(DogParamsSchema), async (req: ValidatedRequest<DogParamsDTO>, res) => {
    const dog = notNullContract(
        await DogODM.findById(req.params.id).exec(),
        new ControllerError('Dog not found', StatusCodes.NOT_FOUND)
    )

    res.json(dog)
})

router.put(
    '/:id/images',
    authenticated(),
    fileStorage.single('image'),
    validator.params(DogParamsSchema),
    async (req: ValidatedRequest<DogImageUploadDTO>, res) => {
        // unfortunately validator.fields(...) does not work for files
        await DogImageUploadSchema.validateAsync(req.file)

        await deleteIfFails(async () => {
            const currentUser = notNullContract(req.user, 'Null user')
            const dog = notNullContract(
                await DogODM.findById(req.params.id).exec(),
                new ControllerError('Dog not found', StatusCodes.NOT_FOUND)
            )

            checkContract(
                dog.user_id.equals(currentUser._id),
                new ControllerError("You cannot upload images for a dog you don't own", StatusCodes.FORBIDDEN)
            )

            const extensions = notNullContract(
                mimedb[req.file.mimetype].extensions,
                new ControllerError('Unknown MIME type', StatusCodes.BAD_REQUEST)
            )

            const filenameWithExtension = `${req.file.filename}.${extensions[0]}`

            await rename(req.file.path, `${req.file.path}.${extensions[0]}`)
            await dog.updateOne({
                $push: {
                    images: filenameWithExtension,
                },
            })

            res.json({ filenameWithExtension })
        }, req.file)
    }
)

router.delete(
    '/:id/images/:imgName',
    authenticated(),
    validator.params(DogImageDeleteSchema),
    async (req: ValidatedRequest<DogImageDeleteDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const dog = notNullContract(
            await DogODM.findById(req.params.id).exec(),
            new ControllerError('Dog not found', StatusCodes.NOT_FOUND)
        )

        checkContract(
            dog.user_id.equals(currentUser._id),
            new ControllerError("You cannot delete images for a dog you don't own", StatusCodes.FORBIDDEN)
        )

        checkContract(
            dog.images.includes(req.params.imgName),
            new ControllerError('Image not found', StatusCodes.NOT_FOUND)
        )

        await dog.updateOne({
            $pull: {
                images: req.params.imgName,
            },
        })

        // TODO: actually delete the file

        res.json({ message: 'deleted' })
    }
)

router.put(
    '/:id',
    authenticated(),
    validator.params(DogParamsSchema),
    validator.body(DogUpdateSchema),
    async (req: ValidatedRequest<DogUpdateDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const dog = notNullContract(
            await DogODM.findById(req.params.id).exec(),
            new ControllerError('Dog not found', StatusCodes.NOT_FOUND)
        )

        checkContract(
            dog.user_id.equals(currentUser._id),
            new ControllerError('You cannot modify a profile for a dog you do not own', StatusCodes.FORBIDDEN)
        )

        const updated = await dog.updateOne(req.body).exec()
        res.json(updated)
    }
)

router.post('/', authenticated(), validator.body(DogCreateSchema), async (req: ValidatedRequest<DogCreateDTO>, res) => {
    const dog = await DogODM.create({
        user_id: notNullContract(req.user, 'Null user').id,
        ...req.body,
    })
    res.json(dog)
})

router.delete(
    '/:id',
    authenticated(),
    validator.params(DogParamsSchema),
    async (req: ValidatedRequest<DogParamsDTO>, res) => {
        const currentUser = notNullContract(req.user, 'Null user')
        const dog = notNullContract(
            await DogODM.findById(req.params.id).exec(),
            new ControllerError('Dog not found', StatusCodes.NOT_FOUND)
        )

        checkContract(
            dog.user_id.equals(currentUser._id),
            new ControllerError('You cannot delete a dog you do not own', StatusCodes.FORBIDDEN)
        )

        await DogODM.findOneAndDelete({ _id: req.params.id }).exec()
        res.json({ message: 'Deleted' })
    }
)

router.get('/breeds', authenticated(), async (req, res) => {
    res.json(BREEDS)
})
