import { Router } from 'express'
import 'express-async-errors'

import { authenticated } from '@app/auth'
import { ChatRoomParamsSchema, ChatRoomQuerySchema, ChatRoomQueryDTO } from '@app/models/dtos/chat'
import validator from '@app/models/dtos/validator'
import { ChatService } from '@app/services/chat'
import { checkContract, notNullContract } from '@app/utils/contracts'
import { ValidatedRequest } from 'express-joi-validation'
import { StatusCodes } from 'http-status-codes'
import { Types } from 'mongoose'
import { ControllerError } from './error'

export function buildRouter(chatService: ChatService): Router {
    const router = Router()

    router.get(
        '/messages/:roomId',
        authenticated(),
        validator.params(ChatRoomParamsSchema),
        async (req: ValidatedRequest<ChatRoomQueryDTO>, res) => {
            req.query = await ChatRoomQuerySchema.validateAsync(req.query, { context: { now: Date() } })

            const currentUser = notNullContract(req.user, 'Null user')
            checkContract(
                await chatService.isInRoom(currentUser._id, req.params.roomId),
                new ControllerError('You are not in this chat room', StatusCodes.FORBIDDEN)
            )

            const messages = await chatService.getMessagesBefore(
                Types.ObjectId(req.params.roomId),
                req.query.date,
                req.query.count
            )
            res.json(messages)
        }
    )

    return router
}
