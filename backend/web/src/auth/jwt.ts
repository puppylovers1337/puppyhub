import { UserDocument, UserODM } from 'src/models/odms/user'
import { ExtractJwt, Strategy as JwtStrategy, VerifiedCallback } from 'passport-jwt'
import { sign } from 'jsonwebtoken'
import { authenticate } from 'passport'
import { ParamsDictionary, RequestHandler } from 'express-serve-static-core'
import { ParsedQs } from 'qs'
import { ExtendedError } from 'socket.io/dist/namespace'
import { Handshake } from 'socket.io/dist/socket'

export interface JwtPayload {
    id: string
    username: string
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const ExtractJwtFromHandshake = (handshake: any): string | null => {
    return (handshake as Handshake).auth['token'] || null
}
export const jwtSocketOptions = {
    jwtFromRequest: ExtractJwtFromHandshake,
    secretOrKey: process.env.JWT_SECRET || 'default',
}

export const jwtHttpOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.JWT_SECRET || 'default',
}

export function verifyJwt(payload: JwtPayload, done: VerifiedCallback): void {
    UserODM.findOne({ username: payload.username }, (err: any, user: UserDocument | null) => {
        if (err) return done(err)
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        done(null, user!)
    })
}

export const getHttpJwtStrategy = (): JwtStrategy => new JwtStrategy(jwtHttpOptions, verifyJwt)
export const getSocketJwtStrategy = (): JwtStrategy => new JwtStrategy(jwtSocketOptions, verifyJwt)

export function signPayload(payload: JwtPayload): string {
    return sign(payload, process.env.JWT_SECRET || 'default')
}

export function jwtHttpMiddleware(): any {
    return authenticate('jwt', { session: false }) as RequestHandler<ParamsDictionary, any, any, ParsedQs>
}

const socketJwtStrategy = getSocketJwtStrategy()
export function jwtSocketMiddleware(socket: SocketExtra, next: (err?: ExtendedError | undefined) => void): void {
    socketJwtStrategy.success = function success(user: Express.User) {
        socket.user = user
        next()
    }
    socketJwtStrategy.authenticate(socket.handshake as any, { session: false })
}
