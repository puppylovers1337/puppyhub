import 'express'
import 'express-async-errors'

import { setupDatabase } from './database'
import buildHttpServer from './httpServer'
import { loadConfig } from './LoadEnv'
import { ChatService } from './services/chat'
import { NotifierService } from './services/notifier'
import buildSocketServer from './socketServer'
import { buildModel as buildChatRoomModel } from '@models/odms/chatRoom'
import { buildModel as MessageModelBuiler } from '@models/odms/message'
import { buildModel as buildMatchModel } from '@models/odms/match'
import buildRouter from './controllers'
import { MatchService } from './services/match'
import { buildExpressApp } from './expressApp'

loadConfig()

const notifierService = new NotifierService()

const ChatRoomODM = buildChatRoomModel(notifierService)
const MessageODM = MessageModelBuiler(ChatRoomODM)
const MatchODM = buildMatchModel(ChatRoomODM)

const chatService = new ChatService(ChatRoomODM, MessageODM)
const matchService = new MatchService(MatchODM)

setupDatabase(MatchODM, MessageODM, ChatRoomODM, chatService)

const baseRouter = buildRouter(chatService, matchService)
const expressApp = buildExpressApp(baseRouter)
const httpServer = buildHttpServer(expressApp)
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const socketServer = buildSocketServer(httpServer, notifierService, chatService)

const port = Number(process.env.PORT || 3000)
httpServer.listen(port, () => {
    console.log('Express server started on port: ' + port)
})
