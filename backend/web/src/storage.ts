import { default as multer } from 'multer'

export const imagesStoragePath = process.env.NODE_ENV === 'test' ? './images_test' : '/data/images'

export const fileStorage = multer({
    dest: imagesStoragePath,
    // TODO: add filters
    // TODO: add size limits
})
