import { Types } from 'mongoose'
import { ChatParticipantInterface } from '@models/interfaces/chatParticipant'
import { MessageDocument, MessageModel } from '@models/odms/message'
import { UserODM } from '@models/odms/user'
import { notNullContract } from '@utils/contracts'
import { ChatRoomDocument, ChatRoomModel } from '@app/models/odms/chatRoom/schema'

export class ChatService {
    constructor(private ChatRoomODM: ChatRoomModel, private MessageODM: MessageModel) {}

    public async createRoom(participants: ChatParticipantInterface[]): Promise<ChatRoomDocument> {
        return await this.ChatRoomODM.create({
            participants,
            lastUpdated: Date.now(),
        })
    }

    public async isInRoom(user_id: Types.ObjectId, roomId: string): Promise<boolean> {
        return await this.ChatRoomODM.exists({
            _id: roomId,
            participants: {
                $elemMatch: {
                    user_id: user_id,
                },
            },
        })
    }

    public async roomsByLastUpdated(user_id: Types.ObjectId): Promise<ChatRoomDocument[]> {
        const user = await UserODM.findById(user_id).exec()
        if (user === null) return []

        return await this.ChatRoomODM.find({ participants: { $elemMatch: { user_id: user_id } } })
            .sort({
                lastUpdated: 'descending',
            })
            .exec()
    }

    public async getLastMessages(room_id: Types.ObjectId, count: number): Promise<MessageDocument[]> {
        const room = await this.ChatRoomODM.findById(room_id).exec()
        if (room === null) return []

        return await this.MessageODM.find({ room_id: room_id }).sort({ createdAt: 'descending' }).limit(count).exec()
    }

    public async getMessages(room_id: Types.ObjectId, count: number): Promise<MessageDocument[]> {
        const room = await this.ChatRoomODM.findById(room_id).exec()
        if (room === null) return []

        return await this.MessageODM.find({ room_id: room_id }).sort({ createdAt: 'descending' }).limit(count).exec()
    }

    public async getMessagesBefore(room_id: Types.ObjectId, date: Date, count: number): Promise<MessageDocument[]> {
        const room = await this.ChatRoomODM.findById(room_id).exec()
        if (room === null) return []

        return await this.MessageODM.find({ room_id: room_id, createdAt: { $lt: date } })
            .sort({ createdAt: 'descending' })
            .limit(count)
            .exec()
    }
    public async saveMessage(
        user_id: Types.ObjectId,
        room_id: Types.ObjectId,
        content: string
    ): Promise<MessageDocument> {
        notNullContract(await UserODM.findById(user_id).exec(), 'User not found')
        notNullContract(await this.ChatRoomODM.findById(room_id).exec(), 'Room not found')

        return await this.MessageODM.create({
            user_id: user_id,
            room_id: room_id,
            content,
        })
    }
}
