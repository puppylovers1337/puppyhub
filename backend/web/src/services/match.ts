import { ControllerError } from '@app/controllers/error'
import { MatchQuery } from '@app/models/dtos/match'
import { MatchStatus } from '@app/models/interfaces/match'
import { DogDocument, DogODM } from '@app/models/odms/dog'
import { MatchModel } from '@app/models/odms/match'
import { UserDocument, UserODM } from '@app/models/odms/user'
import { checkContract, notNullContract } from '@app/utils/contracts'
import { StatusCodes } from 'http-status-codes'

export class MatchService {
    constructor(private MatchODM: MatchModel) {}

    public async findDogsAround(query: MatchQuery, currentUser: UserDocument): Promise<DogDocument[]> {
        return await DogODM.find({
            _id: {
                $nin: currentUser.matchedDogs,
            },
            user_id: {
                $nin: [...currentUser.matchedUsers, currentUser._id],
            },
            location: {
                $near: {
                    $geometry: {
                        type: 'Point',
                        coordinates: [query.latitude, query.longitude],
                    },
                    $maxDistance: query.range * 1000, // KM to M
                },
            },
            ...(query.breed !== undefined && { breed: query.breed }),
            ...(query.sex !== undefined && { sex: query.sex }),
        }).exec()
    }

    public async matchDogs(currentUser: UserDocument, sourceDogId: string, targetDogId: string): Promise<MatchStatus> {
        const sourceDog = notNullContract(
            await DogODM.findById(sourceDogId).exec(),
            new ControllerError('Source dog not found', StatusCodes.NOT_FOUND)
        )

        checkContract(
            sourceDog.user_id.equals(currentUser._id),
            new ControllerError("You don't own the source dog", StatusCodes.FORBIDDEN)
        )

        const targetDog = notNullContract(
            await DogODM.findById(targetDogId).exec(),
            new ControllerError('Target dog not found', StatusCodes.NOT_FOUND)
        )

        const sourceUser = notNullContract(
            await UserODM.findById(sourceDog.user_id),
            new ControllerError('Source user not found', StatusCodes.NOT_FOUND)
        )

        const targetUser = notNullContract(
            await UserODM.findById(targetDog.user_id),
            new ControllerError('Target user not found', StatusCodes.NOT_FOUND)
        )

        const existingMatch = await this.MatchODM.findOne({
            receiverDog_id: sourceDog._id,
            senderDog_id: targetDog._id,
        }).exec()

        if (existingMatch === null) {
            await this.MatchODM.create({
                senderDog_id: sourceDog._id,
                sender_id: sourceDog.user_id,
                receiverDog_id: targetDog._id,
                receiver_id: targetDog.user_id,
            })

            await sourceUser.updateOne({
                $push: {
                    matchedDogs: targetDog._id,
                },
            })

            return MatchStatus.PENDING
        } else if (existingMatch.status === MatchStatus.PENDING) {
            await existingMatch.updateOne({
                status: MatchStatus.ACCEPTED,
            })

            await sourceUser.updateOne({
                $push: {
                    matchedUsers: targetUser._id,
                },
            })
            await targetUser.updateOne({
                $push: {
                    matchedUsers: sourceUser._id,
                },
            })

            return MatchStatus.ACCEPTED
        } else if (existingMatch.status === MatchStatus.DENIED) {
            throw new ControllerError('Already rejected', StatusCodes.FORBIDDEN)
        } else if (existingMatch.status === MatchStatus.ACCEPTED) {
            throw new ControllerError('Already matched', StatusCodes.BAD_REQUEST)
        }

        throw new ControllerError('Invalid match status in db', StatusCodes.INTERNAL_SERVER_ERROR)
    }
}
