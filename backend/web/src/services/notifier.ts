import { Types } from 'mongoose'

export type Listener = (...data: unknown[]) => Promise<void>

export function newChatRoomForUserEvent(user_id: Types.ObjectId): string {
    return `NEW_CHAT_ROOM_FOR_${user_id.toHexString()}`
}

export class NotifierService {
    subscriptions: {
        [event: string]: Listener[]
    } = {}

    registerEvent(event: string): void {
        if (event in this.subscriptions) return
        this.subscriptions[event] = []
    }

    emitEvent(event: string, ...data: unknown[]): void {
        if (!(event in this.subscriptions)) return
        this.subscriptions[event].forEach((listener) => {
            listener(...data)
        })
    }

    watchEvent(event: string, listener: Listener): Listener {
        this.registerEvent(event)
        this.subscriptions[event].push(listener)
        return listener
    }

    unwatchEvent(event: string, listener: Listener): void {
        if (!(event in this.subscriptions)) return
        const index = this.subscriptions[event].indexOf(listener)
        if (index === -1) return
        this.subscriptions[event].splice(index, 1)
    }
}
