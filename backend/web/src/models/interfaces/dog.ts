import { Types } from 'mongoose'
import { PointInterface } from './location'

export interface DogInterface {
    user_id: Types.ObjectId
    name: string
    breed: string
    location: PointInterface
    sex: Sex
    images: string[]
    description: string
}

export enum Sex {
    MALE = 'male',
    FEMALE = 'female',
}
