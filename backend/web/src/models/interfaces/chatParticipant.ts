import { Types } from 'mongoose'

export interface ChatParticipantInterface {
    user_id: Types.ObjectId
    dog_id: Types.ObjectId
    displayName: string
    image?: string
}
