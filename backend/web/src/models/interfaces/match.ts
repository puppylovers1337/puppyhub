import { Types } from 'mongoose'

export interface MatchInterface {
    sender_id: Types.ObjectId
    receiver_id: Types.ObjectId
    senderDog_id: Types.ObjectId
    receiverDog_id: Types.ObjectId
    status: MatchStatus
}

export enum MatchStatus {
    PENDING = 'PENDING',
    ACCEPTED = 'ACCEPTED',
    DENIED = 'DENIED',
}
