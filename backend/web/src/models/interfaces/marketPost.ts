import { Types } from 'mongoose'

export interface MarketPostInterface {
    user_id: Types.ObjectId
    images: string[]
    description: string
    price: string
    type: MarketPostType
    location: string
    email: string
    phoneNumber: string
}

export enum MarketPostType {
    SELL = 'SELL',
    BABYSIT = 'BABYSIT',
}

export const MarketPostTypes = Object.values(MarketPostType)
