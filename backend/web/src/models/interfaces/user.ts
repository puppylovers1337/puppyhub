import { ProfileInterface } from './profile'
import { Types } from 'mongoose'

export interface UserInterface {
    username: string
    password: string
    email: string
    role: UserRole
    profile: ProfileInterface
    matchedDogs: Types.ObjectId[]
    matchedUsers: Types.ObjectId[]
}

export enum UserRole {
    DEFAULT = 'DEFAULT',
    ADMIN = 'ADMIN',
}
export const UserRoles = Object.values(UserRole)
