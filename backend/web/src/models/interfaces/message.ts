import { Types } from 'mongoose'

export interface MessageInterface {
    room_id: Types.ObjectId
    user_id: Types.ObjectId
    content: string
    createdAt: Date
}
