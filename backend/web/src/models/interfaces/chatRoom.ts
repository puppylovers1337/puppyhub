import { ChatParticipantInterface } from './chatParticipant'

export interface ChatRoomInterface {
    lastUpdated: Date
    participants: ChatParticipantInterface[]
}

export const DEFAULT_MESSAGE_HISTORY_SIZE = 15
