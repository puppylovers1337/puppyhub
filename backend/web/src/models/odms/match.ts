import { notNullContract } from '@app/utils/contracts'
import { Document, Schema, Model, model, connection } from 'mongoose'
import { MatchInterface, MatchStatus } from '../interfaces/match'
import { ChatRoomModel } from './chatRoom/schema'
import { DogODM } from './dog'

export interface MatchDocument extends MatchInterface, Document {}

export type MatchModel = Model<MatchDocument>

export const MatchSchema = new Schema({
    sender_id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    receiver_id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    senderDog_id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    receiverDog_id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    status: {
        type: String,
        enum: Object.values(MatchStatus),
        required: true,
        default: MatchStatus.PENDING,
    },
})

export function buildModel(ChatRoomODM: ChatRoomModel): MatchModel {
    if (connection.models['Match']) throw new Error('Mongoose supports only one model instance')

    MatchSchema.post('updateOne', async function (doc, next) {
        const q = this as any
        // TODO: types for this
        const matchId = q._conditions._id
        if (q._update.$set.status === MatchStatus.ACCEPTED) {
            const matchDoc: MatchDocument = notNullContract(await q.model.findById(matchId).exec(), 'Null match')
            const senderDog = notNullContract(await DogODM.findById(matchDoc.senderDog_id).exec(), 'Null source (dog)')
            const receiverDog = notNullContract(
                await DogODM.findById(matchDoc.receiverDog_id).exec(),
                'Null target (dog)'
            )

            await ChatRoomODM.create({
                participants: [
                    {
                        displayName: senderDog.name,
                        dog_id: senderDog._id,
                        user_id: senderDog.user_id,
                        image: senderDog.images.length > 0 ? senderDog.images[senderDog.images.length - 1] : null,
                    },
                    {
                        displayName: receiverDog.name,
                        dog_id: receiverDog._id,
                        user_id: receiverDog.user_id,
                        image: receiverDog.images.length > 0 ? receiverDog.images[receiverDog.images.length - 1] : null,
                    },
                ],
                lastUpdated: Date.now(),
            })
        }
        next()
    })

    return model<MatchDocument>('Match', MatchSchema)
}
