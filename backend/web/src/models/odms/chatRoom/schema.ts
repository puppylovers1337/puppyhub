import { Document, Model, Schema } from 'mongoose'
import { ChatRoomInterface } from '../../interfaces/chatRoom'
import { ChatParticipantSchema } from '../chatParticipant'

export interface ChatRoomDocument extends ChatRoomInterface, Document {}

export type ChatRoomModel = Model<ChatRoomDocument>

export const ChatRoomSchema = new Schema<ChatRoomDocument, ChatRoomModel>({
    lastUpdated: {
        type: Date,
        required: true,
    },
    participants: [ChatParticipantSchema],
})
