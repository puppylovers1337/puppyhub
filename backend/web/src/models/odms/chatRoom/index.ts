import { newChatRoomForUserEvent, NotifierService } from '@app/services/notifier'
import { connection, model } from 'mongoose'
import { ChatRoomDocument, ChatRoomModel, ChatRoomSchema } from './schema'

export function buildModel(notifierService: NotifierService): ChatRoomModel {
    if (connection.models['ChatRoom']) throw new Error('Mongoose supports only one model instance of ChatRoom')

    ChatRoomSchema.post('save', function () {
        this.participants.forEach((participant) => {
            notifierService.emitEvent(newChatRoomForUserEvent(participant.user_id), this)
        })
    })

    return model<ChatRoomDocument, ChatRoomModel>('ChatRoom', ChatRoomSchema)
}
