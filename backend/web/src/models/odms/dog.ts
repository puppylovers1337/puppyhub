import { BREEDS } from 'data/breeds'
import { Schema, Document, model } from 'mongoose'
import { DogInterface, Sex } from '../interfaces/dog'

export interface DogDocument extends Document, DogInterface {}

export const DogSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        require: true,
    },
    name: {
        type: String,
        required: true,
    },
    breed: {
        type: String,
        enum: BREEDS,
        required: true,
    },
    location: {
        type: { type: String },
        coordinates: [],
    },
    images: {
        type: [String],
        required: true,
        default: [],
    },
    sex: {
        type: Sex,
        required: true,
    },
    description: {
        type: String,
        default: 'The Best Doggo',
    },
})

DogSchema.index({ location: '2dsphere' })
export const DogODM = model<DogDocument>('Dog', DogSchema)
DogODM.createIndexes()
