import { Schema, Document, model, Model } from 'mongoose'
import { MarketPostInterface, MarketPostType } from '../interfaces/marketPost'

export interface MarketPostDocument extends Document, MarketPostInterface {}

export const MarketPostSchema = new Schema(
    {
        user_id: {
            type: Schema.Types.ObjectId,
            require: true,
        },
        title: {
            type: String,
            reqired: true,
        },
        description: {
            type: String,
            required: true,
        },
        price: {
            type: String,
            default: 'Negociable',
        },
        type: {
            type: MarketPostType,
            default: MarketPostType.SELL,
        },
        images: {
            type: [String],
            required: true,
            default: [],
        },
        location: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        phoneNumber: {
            type: String,
            reqired: true,
        },
    },
    { timestamps: true }
)

export type MarketPostModel = Model<MarketPostDocument>

export const MarketPostODM = model<MarketPostDocument>('MarketPost', MarketPostSchema)
