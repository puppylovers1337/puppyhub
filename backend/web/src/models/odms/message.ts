import { connection, Document, model, Model, Schema } from 'mongoose'
import { MessageInterface } from '../interfaces/message'
import { ChatRoomModel } from './chatRoom/schema'

export interface MessageDocument extends MessageInterface, Document {}

export type MessageModel = Model<MessageDocument>

export const MessageSchema = new Schema(
    {
        room_id: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        user_id: {
            type: Schema.Types.ObjectId,
            required: true,
        },
        content: String,
    },
    { timestamps: true }
)

export function buildModel(ChatRoomODM: ChatRoomModel): MessageModel {
    if (connection.models['Message']) throw new Error('Mongoose supports only one model instance')

    MessageSchema.post('save', async function (doc, next) {
        const message = doc as MessageDocument
        const room = await ChatRoomODM.findById(message.room_id).exec()
        if (room === null) {
            next()
            return
        }

        await room.updateOne({ lastUpdated: message.createdAt }).exec()
        next()
    })

    return model<MessageDocument, MessageModel>('Message', MessageSchema)
}
