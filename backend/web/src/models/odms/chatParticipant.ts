import { Model, Document, Schema, model } from 'mongoose'
import { ChatParticipantInterface } from '../interfaces/chatParticipant'

export interface ChatParticipantDocument extends ChatParticipantInterface, Document {}

export type ChatParticipantModel = Model<ChatParticipantDocument>

export const ChatParticipantSchema = new Schema({
    user_id: {
        type: Schema.Types.ObjectId,
        required: true,
    },
    dog_id: {
        type: Schema.Types.ObjectId,
    },
    displayName: {
        type: String,
        required: true,
    },
    image: {
        type: String,
    },
})

export const ChatParticipantODM = model<ChatParticipantDocument, ChatParticipantModel>(
    'ChatParticipant',
    ChatParticipantSchema
)
