import * as Joi from 'joi'
import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation'
import { DogInterface, Sex } from '../interfaces/dog'
import { LocationSchema } from './location'
import { BREEDS } from 'data/breeds'

export const DogCreateSchema = Joi.object({
    name: Joi.string().required().max(20),
    breed: Joi.string()
        .valid(...BREEDS)
        .required(),
    location: LocationSchema.required(),
    sex: Joi.string().valid(Sex.FEMALE, Sex.MALE).required(),
    description: Joi.string().optional(),
})

export const DogUpdateSchema = Joi.object({
    user_id: Joi.string().optional(),
    name: Joi.string().optional().max(20),
    breed: Joi.string()
        .valid(...BREEDS)
        .optional(),
    location: LocationSchema.optional(),
    sex: Joi.string().valid(Sex.FEMALE, Sex.MALE).optional(),
    description: Joi.string().optional(),
})

export const DogParamsSchema = Joi.object({
    id: Joi.string().required(),
})

export const DogImageUploadSchema = Joi.object().required().messages({
    'any.required': 'File is required',
})

export const DogImageDeleteSchema = Joi.object({
    id: Joi.string().required(),
    imgName: Joi.string().required(),
})

export interface DogCreateDTO extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<DogInterface, 'user_id'>
}

export interface DogParamsDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
}

export interface DogUpdateDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
    [ContainerTypes.Body]: Partial<DogInterface>
}

export interface DogImageUploadDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
}

export interface DogImageDeleteDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
        imgName: string
    }
}
