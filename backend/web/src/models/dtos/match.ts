import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import Joi from 'joi'
import { Sex } from '../interfaces/dog'

export const MatchQuerySchema = Joi.object({
    longitude: Joi.number().required(),
    latitude: Joi.number().required(),
    range: Joi.number().required(),
    breed: Joi.string().optional(),
    sex: Joi.string().valid(Sex.FEMALE, Sex.MALE).optional(),
})

export const MatchCreateSchema = Joi.object({
    targetDogId: Joi.string().required(),
    sourceDogId: Joi.string().required(),
})

export interface MatchQuery {
    longitude: number
    latitude: number
    range: number
    breed: string
    sex?: Sex
}

export interface MatchQueryDTO extends ValidatedRequestSchema {
    [ContainerTypes.Query]: MatchQuery
}

export interface MatchCreate {
    targetDogId: string
    sourceDogId: string
}

export interface MatchCreateDTO extends ValidatedRequestSchema {
    [ContainerTypes.Body]: MatchCreate
}
