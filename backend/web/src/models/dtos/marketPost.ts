import * as Joi from 'joi'
import { MarketPostInterface, MarketPostTypes } from '../interfaces/marketPost'
import { ValidatedRequestSchema, ContainerTypes } from 'express-joi-validation'

export const MarketPostCreateSchema = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().optional(),
    price: Joi.string().optional(),
    type: Joi.string()
        .valid(...MarketPostTypes)
        .required(),
    location: Joi.string().required(),
    email: Joi.string().required(),
    phoneNumber: Joi.string().required(),
})

export const MarketPostImageUploadSchema = Joi.object().required().messages({
    'any.required': 'File is required',
})

export const MarketPostImageDeleteSchema = Joi.object({
    id: Joi.string().required(),
    imgName: Joi.string().required(),
})

export const MarketPostUpdateSchema = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().optional(),
    price: Joi.string().optional(),
    type: Joi.string()
        .valid(...MarketPostTypes)
        .optional(),
    location: Joi.string().required(),
    email: Joi.string().required(),
    phoneNumber: Joi.string().required(),
})

export const MarketPostParamsSchema = Joi.object({
    id: Joi.string().required(),
})

export interface MarketPostParamsDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
}

export interface MarketPostUpdateDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
    [ContainerTypes.Body]: Partial<MarketPostInterface>
}

export interface MarketPostCreateDTO extends ValidatedRequestSchema {
    [ContainerTypes.Body]: Omit<MarketPostInterface, 'user_id'>
}

export interface MarketPostImageUploadDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
    }
}

export interface MarketPostImageDeleteDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        id: string
        imgName: string
    }
}
