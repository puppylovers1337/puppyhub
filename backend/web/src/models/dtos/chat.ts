import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation'
import Joi from 'joi'
import { DEFAULT_MESSAGE_HISTORY_SIZE } from '../interfaces/chatRoom'

export const ChatRoomParamsSchema = Joi.object({
    roomId: Joi.string().required(),
})

export const ChatRoomQuerySchema = Joi.object({
    count: Joi.number().integer().default(DEFAULT_MESSAGE_HISTORY_SIZE).optional(),
    date: Joi.date().default(Joi.ref('$now')).optional(),
})

export interface ChatRoomQueryDTO extends ValidatedRequestSchema {
    [ContainerTypes.Params]: {
        roomId: string
    }
    [ContainerTypes.Query]: {
        count: number
        date: Date
    }
}
