from requests import get
from os import system
from datetime import datetime

breeds = get('https://dog.ceo/api/breeds/list/all').json()['message']

result = []
for k, v in breeds.items():
    if len(v) == 0: 
        result.append(k)
    else:
        result.extend(k + " " + x for x in v)

with open('data/breeds.ts', 'w') as f:
    f.write(f"""\
// DO NOT MODIFY! THIS FILE WAS AUTO-GENERATED!
// {datetime.now()} 
// 
//
// Thanks to Dog CEO for making this data publicly. 
// Check it out here https://dog.ceo/dog-api/
// 
""")
    f.write(f"export const BREEDS = {result}")

system("npm run lint")