import { ChatParticipantInterface } from '@models/interfaces/chatParticipant'
import { UserDocument, UserODM } from '@models/odms/user'
import { Types } from 'mongoose'
import { expect, assert } from 'chai'
import { notNullContract } from '@utils/contracts'
import { ChatRoomDocument } from '@app/models/odms/chatRoom/schema'
import { ChatRoomODM, chatService } from './util.spec'

describe('Chat service', () => {
    const users: UserDocument[] = []
    let participants: ChatParticipantInterface[] = []

    before(async () => {
        users.push(
            await UserODM.create({
                username: 'AAAA',
                email: 'AAAA@puppyhub.com',
                password: 'AAAA',
            })
        )
        users.push(
            await UserODM.create({
                username: 'BBBB',
                email: 'BBBB@puppyhub.com',
                password: 'BBBB',
            })
        )

        participants = users.map((user, i) => ({
            user_id: user._id,
            dog_id: new Types.ObjectId(i),
            displayName: user.username,
        }))
    })

    it('should save messages', async () => {
        const room = await chatService.createRoom(participants)
        const message = await chatService.saveMessage(users[0]._id, room._id, 'AAAA')
        expect(message.content).equals('AAAA')
    })

    it('should retreive messages in the right order', async () => {
        const room = await chatService.createRoom(participants)
        const message = await chatService.saveMessage(users[1]._id, room._id, 'BBBB')
        const messages = await chatService.getLastMessages(room._id, 1)
        expect(messages[0].content).equals(message.content)
    })

    it("should update room's timestamp", async () => {
        const room = await chatService.createRoom(participants)
        const message = await chatService.saveMessage(users[0]._id, room._id, 'BBBB')
        const updatedRoom = notNullContract(await ChatRoomODM.findOne(room._id).exec(), 'Error')
        assert.deepEqual(message.createdAt, updatedRoom.lastUpdated)
    })

    it('should return most recent room', async () => {
        const rooms: ChatRoomDocument[] = []
        for (let i = 0; i < 3; i++) {
            rooms.push(await chatService.createRoom(participants))
        }

        const mostRecent1 = await chatService.roomsByLastUpdated(users[0]._id)
        expect(rooms[2].id).equals(mostRecent1[0].id)

        await chatService.saveMessage(users[1]._id, rooms[1]._id, 'CCCC')
        const mostRecent2 = await chatService.roomsByLastUpdated(users[0]._id)
        expect(rooms[1].id).equals(mostRecent2[0].id)
    })
})
