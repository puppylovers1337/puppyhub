import { ChatService } from '@app/services/chat'
import { MatchService } from '@app/services/match'
import { buildModel as buildChatRoomModel } from '@app/models/odms/chatRoom'
import { buildModel as buildMessageModel } from '@app/models/odms/message'
import { buildModel as buildMatchModel } from '@app/models/odms/match'
import { NotifierService } from '@app/services/notifier'
import buildRouter from '@app/controllers'
import { buildExpressApp } from '@app/expressApp'
import buildHttpServer from '@app/httpServer'

export const notifierService = new NotifierService()
export const ChatRoomODM = buildChatRoomModel(notifierService)
export const MessageODM = buildMessageModel(ChatRoomODM)
export const MatchODM = buildMatchModel(ChatRoomODM)
export const chatService = new ChatService(ChatRoomODM, MessageODM)
export const matchService = new MatchService(MatchODM)
export const baseRouter = buildRouter(chatService, matchService)
export const expressApp = buildExpressApp(baseRouter)
export const httpServer = buildHttpServer(expressApp)
