import { Sex } from '@app/models/interfaces/dog'
import { PointInterface } from '@app/models/interfaces/location'
import { MatchStatus } from '@app/models/interfaces/match'
import { DogDocument } from '@app/models/odms/dog'
import { UserDocument, UserODM } from '@app/models/odms/user'
import { notNullContract } from '@app/utils/contracts'
import { fakeDog, fakeUsers } from '@app/utils/faker'
import { expect } from 'chai'
import { chatService, matchService } from './util.spec'

describe('Match', () => {
    const users: UserDocument[] = []
    const dogs: DogDocument[] = []

    const defaultLocation: PointInterface = {
        type: 'Point',
        coordinates: [25, 26],
    }
    // 1 latitude ~ 100 km
    const shiftedLatLocation: PointInterface = {
        type: 'Point',
        coordinates: [26, 26],
    }

    before(async () => {
        users.push(...(await fakeUsers(3)))
    })

    step('should return dogs around', async () => {
        dogs.push(await fakeDog(users[0], defaultLocation, Sex.MALE))
        const result = await matchService.findDogsAround(
            {
                breed: dogs[0].breed,
                latitude: shiftedLatLocation.coordinates[0],
                longitude: shiftedLatLocation.coordinates[1],
                range: 150,
                sex: Sex.MALE,
            },
            users[1]
        )
        expect(result.length).equals(1)
        expect(result[0].id).equals(dogs[0].id)
    })
    step('should return no dogs', async () => {
        const result = await matchService.findDogsAround(
            {
                breed: dogs[0].breed,
                latitude: shiftedLatLocation.coordinates[0],
                longitude: shiftedLatLocation.coordinates[1],
                range: 50,
                sex: Sex.MALE,
            },
            users[1]
        )
        expect(result.length).equals(0)
    })
    step('should exclude dogs of the current user', async () => {
        const currentUser = users[1]
        dogs.push(await fakeDog(currentUser, defaultLocation, Sex.FEMALE))

        const result = await matchService.findDogsAround(
            {
                breed: dogs[1].breed,
                latitude: shiftedLatLocation.coordinates[0],
                longitude: shiftedLatLocation.coordinates[1],
                range: 150,
                sex: Sex.FEMALE,
            },
            currentUser
        )
        expect(result.length).equals(0)
    })
    step('should exclude already matched dogs', async () => {
        dogs.push(await fakeDog(users[2], defaultLocation, Sex.MALE))

        let currentUser = users[1]
        await currentUser.updateOne({
            $push: {
                matchedDogs: dogs[2]._id,
            },
        })
        users[1] = notNullContract(await UserODM.findById(users[1]._id), 'User null')
        currentUser = users[1]

        const result = await matchService.findDogsAround(
            {
                breed: dogs[1].breed,
                latitude: shiftedLatLocation.coordinates[0],
                longitude: shiftedLatLocation.coordinates[1],
                range: 150,
                sex: Sex.MALE,
            },
            currentUser
        )
        expect(result.length).equals(1)
        expect(result[0].id).equals(dogs[0].id)
    })

    step('should match two dogs', async () => {
        const sourceDog = dogs[0]
        const targetDog = dogs[1]
        const currentUser = users[0]
        const otherUser = users[1]

        const status1 = await matchService.matchDogs(currentUser, sourceDog.id, targetDog.id)
        expect(status1).equals(MatchStatus.PENDING)

        const status2 = await matchService.matchDogs(otherUser, targetDog.id, sourceDog.id)
        expect(status2).equals(MatchStatus.ACCEPTED)

        const chatRooms = await chatService.roomsByLastUpdated(currentUser._id)
        expect(chatRooms.length).equals(1)
    })

    step('should not see already matched user', async () => {
        const result = await matchService.findDogsAround(
            {
                breed: dogs[0].breed,
                latitude: shiftedLatLocation.coordinates[0],
                longitude: shiftedLatLocation.coordinates[1],
                range: 300,
                sex: Sex.MALE,
            },
            users[0]
        )

        expect(result.length).is.greaterThan(0)
        result.forEach((dog) => expect(dog.user_id).is.not.equal(users[1]._id))
    })
})
