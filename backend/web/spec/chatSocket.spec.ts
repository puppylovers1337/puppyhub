import { UserDocument, UserODM } from '@app/models/odms/user'
import socketHandler, { ErrorMessage, Events } from '@app/sockets'
import { strictEqual } from 'assert'
import { default as chai, expect } from 'chai'
import { io as Client, Socket as ClientSocket } from 'socket.io-client'
import chaiHttp from 'chai-http'
import { Server as SocketIOServer, Socket } from 'socket.io'
import { createServer, Server } from 'http'
import { jwtSocketMiddleware } from '@app/auth/jwt'
import { Types } from 'mongoose'
import { ChatRoomDocument } from '@app/models/odms/chatRoom/schema'
import { chatService, httpServer, notifierService } from './util.spec'

chai.use(chaiHttp)

describe('Chat live socket', () => {
    let clientSocket: ClientSocket | null = null
    let socketIOServer: SocketIOServer | null = null
    let localHttpServer: Server | null = null

    // BUG: for unkown reasons, chai-http breaks socketio so
    // we must listen on different ports
    const defaultHttpPort = 5000
    const defaultSocketPort = 5001

    const userData = {
        username: 'socket_AAAA',
        email: 'socket_AAAA@puppyhub.com',
        password: '1234',
    }
    let user: UserDocument | null = null
    let room: ChatRoomDocument | null = null
    let token: string | null = null

    before((done) => {
        localHttpServer = createServer()
        socketIOServer = new SocketIOServer(localHttpServer)
        socketIOServer.use(jwtSocketMiddleware)
        socketIOServer.on('connection', (sock: Socket) =>
            // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
            socketHandler(sock, chatService, socketIOServer!, notifierService)
        )

        httpServer.listen(defaultHttpPort, async () => {
            user = await UserODM.create(userData)
            room = await chatService.createRoom([
                {
                    user_id: user._id,
                    displayName: user.username,
                    dog_id: Types.ObjectId(0),
                },
            ])

            const response = await chai.request(httpServer).post('/auth/login').send({
                username: userData.username,
                password: userData.password,
            })
            token = response.body['token'] as string

            localHttpServer?.listen(defaultSocketPort, () => {
                clientSocket = Client(`ws://localhost:${defaultSocketPort}`, {
                    auth: {
                        token,
                    },
                })
                clientSocket?.on('connect', done)
            })
        })
    })

    step('should connect to socket', () => {
        strictEqual(true, true)
    })

    step('should echo back', (done) => {
        clientSocket?.on(Events.ECHO, (msg: string) => {
            expect(msg).equals('some text')
            done()
        })
        clientSocket?.emit(Events.ECHO, 'some text')
    })

    step('should receive rooms', (done) => {
        clientSocket?.on(Events.SYNC, (rooms: any[]) => {
            expect(rooms.length).equals(1)
            expect(rooms[0]._id).equal(room?.id)
        })
        clientSocket?.on(Events.LIVE, done)
        clientSocket?.emit(Events.SYNC)
    })

    step('should send and receive message', (done) => {
        clientSocket?.on(Events.MESSAGE, (msg: any) => {
            ;(room as ChatRoomDocument).lastUpdated = msg.createdAt
            expect(msg.content).equals('some text')
            expect(Date.parse(msg.createdAt as string)).lessThan(Date.now())
            done()
        })
        clientSocket?.emit(Events.MESSAGE, { roomId: room?.id, content: 'some text' })
    })

    step('should not be able to SYNC twice', (done) => {
        clientSocket?.off(Events.SYNC)
        clientSocket?.on(Events.SYNC, (msg: ErrorMessage) => {
            expect(msg.error).equals(true)
            expect(msg.message).equal('Already synchronized')
            done()
        })
        clientSocket?.emit(Events.SYNC, [])
    })

    step('should send NEW_ROOM', (done) => {
        if (user === null) throw new Error('Fail')
        clientSocket?.on(Events.NEW_ROOM, (room: ChatRoomDocument) => {
            expect(room).to.not.be.undefined
            expect(room).to.not.be.null
            expect(room?.participants[0].user_id).equals(user?.id)
            done()
        })

        chatService.createRoom([
            {
                user_id: user._id,
                displayName: user.username,
                dog_id: Types.ObjectId(0),
            },
        ])
    })

    step('should not be able to MESSAGE before SYNC', (done) => {
        clientSocket?.disconnect()
        clientSocket?.off(Events.MESSAGE)
        clientSocket?.off(Events.SYNC)
        clientSocket?.off(Events.LIVE)

        clientSocket = Client(`ws://localhost:${defaultSocketPort}`, {
            auth: {
                token,
            },
        })

        clientSocket?.on(Events.MESSAGE, (msg: ErrorMessage) => {
            expect(msg.error).equals(true)
            expect(msg.message).equal('You are not live yet')
            done()
        })
        clientSocket?.on('connect', () => {
            clientSocket?.emit(Events.MESSAGE)
        })
    })

    step('should receive error on invalid MESSAGE', (done) => {
        clientSocket?.off(Events.MESSAGE)
        clientSocket?.off(Events.SYNC)
        clientSocket?.off(Events.LIVE)

        clientSocket?.on(Events.MESSAGE, (msg: ErrorMessage) => {
            expect(msg.error).equals(true)
            expect(msg.message).equals('Invalid message format')
            done()
        })
        clientSocket?.on(Events.LIVE, () => {
            clientSocket?.emit(Events.MESSAGE)
        })
        clientSocket?.emit(Events.SYNC, [])
    })

    after(() => {
        clientSocket?.close()
        socketIOServer?.close()
        localHttpServer?.close()
        httpServer.close()
    })
})
