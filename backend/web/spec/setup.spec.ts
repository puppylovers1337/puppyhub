import mongoose from 'mongoose'
import { MongoMemoryServer } from 'mongodb-memory-server'
const mongoServer = new MongoMemoryServer()

before(async () => {
    mongoose.Promise = Promise
    const mongooseOpts = {
        autoReconnect: false,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    }

    const mongoUri = await mongoServer.getUri()
    await mongoose.connect(mongoUri, mongooseOpts)
})

after(async () => {
    await mongoose.connection.close()
    await mongoServer.stop()
})
