## Building

- run `npm install`
- make sure you have `docker` and `docker-compose` installed
- run `./dev.bat` for Windows or `./dev.sh` for Linux and the API should be exposed on localhost:3000
