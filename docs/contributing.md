How can you contribute?
===

1. You can find current tasks on the [boards](https://gitlab.com/puppylovers1337/puppyhub/-/boards).
2. If you are working on a feature, please create a separate branch or use a trunk branch like `frontend_dev` or `backend_dev` where everyone is pushing.
3. Always `pull` or `rebase` your local repo. Never `push --force`!!!
4. Create a merge request for your feature branch if it is the case.
