Development process report
===

Our project aims to create a friendly environment for dog owners to find walking and romantic partners for their beloved pets. In the past months, we have successfully built a mobile application that connects pet owners in their close vicinity among other great features. This report is going to describe our exciting journey in designing and developing this product.

## The Brainstorm

If you don't spend an enormous amount of time in endless, off-topic meetings then you're doing something wrong for sure. As we value good practice, we've done exactly that from time to time until our glorious idea emerged. Afterward, we established a common ground for preferred technologies and work methodologies before jumping into actually building something.

![Meetings Document](./meetings.png)
![Meetings Technology Stack](./meeting_techs.png)

## From messy brains to messy boards

We decided to use Gitlab as our primary management tool for issue tracking, requirements, documentation, and code. For tracking user stories and their status we are using the Gitlab Requirements feature, you can check the live list [here](https://gitlab.com/puppylovers1337/puppyhub/-/requirements_management/requirements).

![Requirements](./requirements.png)

For managing the backlog of the project with tasks and bugs that arise over time, Github Boards is the perfect choice as it is integrated with the versioning system directly.

![Boards](./board_7_may.png)

## Design is the one and only fun documentation

UI design and system architecture were two parallel processes on which we've spent time in the following days. For designing the user interface we chose Figma as it allows rapid prototyping.

![Figma](./figma_design_done.png)

System architecture diagrams were drawn using Whimsical, a collaborative drawing tool.

![System context](./puppyhub_system_context.png)
![Backend context](./puppyhub_backend_context.png)

The tech stack for each part of the system is the following:
1.  Flutter & Dart for mobile and web development
2.  Flutter Provider for state management on the front-end
3.  SocketIO for handling live communication
4.  Express & Typescript for back-end API development
5.  Passport for authentication and authorization
6.  Mocha and Chai for API unit and integration testing
7.  JOI for data validation
8.  MongoDB and Mongoose for data storage and management
9.  Docker for deployment and development
10.  Typescript Compiler API for generating documentation

## The highest branch is not the safest roost

Writing code should tell a story and for tracking our story we are using Git hosted on Gitlab. We adopted a simple methodology for using branches and integrating our changes:
-   master - principal branch, push here only urgent fixes and merge feature branches into it
-   frontend_dev - trunk branch, as front-end development seemed highly volatile we used a trunk branch instead of multiple feature branches, rebasing and merging it into master from time to time
-   feature branches - separate branch for individual and well-defined features
-   branches are merged only if the CI succeeds and it received at least one review

Note: we squashed most of our commits from other branches than master and Gitlab does not understand co-authoring so well, that's why some of the team members do not appear in the statistics and the main branch

![Git Branches](./branches.png)

## Tests are useless until you break production

We are using a combination of unit and integration testing to check that critical parts of our application work as expected and future features do not break the current ones.

![Testing](./testing.png)
![Pipeline](./pipeline.png)

## It's not a bug, it's a feature

As stated before, bug reporting, tracking, and fixing are hosted by Gitlab.

![Bug Tracking](./bugs.png)

## Digital Masons

On the front-end side of things, we used Flutter's default build system, which under the hood uses Gradle. On the back-end, we used NPM Scripts and the features of TypeScript Compiler.

![Build Front-end](./build_frontend.png)
![Build Back-end](./build_backend.png)

## Refactoring or overengineering what's already working

While building the live chat we realized that the amount of code and complexity was growing at the speed of light so we made a step back. We analyzed the existing solutions and patterns for managing live connections and shared state between multiple components then refactored the entire chat implementation. We ended up removing almost 600 lines of useless code.

![Refactor commit](./refactor.png)

## Coding clichés

Among other interesting patterns used during development, the most useful was dependency injection. It allows elegantly expressing dependencies of separate modules and eases mocking and testing. There won't be any CI without it.

![Dependency injection](./dep_inj.png)

## Final thoughts

Please write an appropriate conclusion. One must imagine Sisyphus happy.
