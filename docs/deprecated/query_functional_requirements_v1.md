Query requirements
===
* Get user by identifier
* Get profile of user by identifier (embedded document)
* Get all dogs of user (if by username -> 1 join)
* Get dogs of breed X in range of Y km except already matched users and dogs (location in dog collection, no joins, already matched ids are in user collection)
* Half match from X to Y (add matched dog id to exception list)
* Complete match from Y to X (change status, add user id to exception list, trigger chat room creation)
* Create chat room from X to Y (add all details about participants like photo, name, etc)
* Display chat rooms by date (cache all details on frontend)
* Get last N messages from chat room X (details for displaying already cached)
* Send message to chat room X (update chat room last updated date)

![Provisional diagram](./docs/provisional_diagram.png)
