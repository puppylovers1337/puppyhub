# Chat protocol

This is a description of how a client should interact with the chat server using **SocketIO**.

1. Connect to the server using a SocketIO client library on `http://<hostname>:<port>/`. Add to the client's options `{ "auth": { "token": "<your login token>" } }`, otherwise your access would be denied.
2. After connecting, you should emit a `sync` event. You must pass a list of existing stored rooms. `io.emit('sync', [... list of room objects ...])` (see the model `ChatRoom`)
3. Listen for a reply for `sync` event. The server will send you the list of rooms and new messages you have to store. `io.on('sync', ([... list of rooms and messages ...]) => { ... update local store ... })`
4. Wait for `live` event. After receving it, you are connected to the live chat.
5. You will receive messages on `message` event. Messages are `Message` models. After receiving a message please update its corresponding room timestamp in your storage (this way you'll know what's your last message when you'll `sync`). `rooms[new_message.room_id].lastUpdated = new_message.createdAt`
6. To send a message, emit a `message` event and pass an argument of type `SocketMessage`. Don't store your message directlly! Wait until the server will resend it to you as a `message` event.

### Models

```typescript
export enum Events {
  ECHO = "echo", // reply with the same message
  MESSAGE = "message", // send message to a specified room
  SYNC = "sync", // synchronize rooms and messages
  LIVE = "live", // you're live!
}

export interface ChatRoomSync {
  chatRoom: ChatRoomDocument;
  messages: MessageDocument[];
}

export interface SocketMessage {
  roomId: string;
  content: string;
}
```
