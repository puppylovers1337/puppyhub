Technical documentation
===

1. [How to run the project?](./development.md)
2. [How to contribute?](./contributing.md)
3. Backend API
    1. [Script for generation](../backend/web/src/gendoc.ts)
    2. Swagger: https://app.swaggerhub.com/apis-docs/JustBeYou/PuppyHub/0.0.3
    3. [Development guide for backend](../backend/web/README.md) - here you can find how to develop new features for the API
    4. [Building and running the backend](../backend/README.md)
4. Frontend
    1. Workflow https://whimsical.com/puppy-hub-design-F2BmRQqTrXbhJFdDhLSqas
    2. Notes https://docs.google.com/document/d/1s2fE3joh5u83p0MN2rJ3pS18-mrA1Bpw0Syh3Oema_M/edit
    3. Figma design https://www.figma.com/file/QPB8KX8uW2wCpyNZTu55hX/App-Concept?node-id=0%3A1
5. [Chat protocol](./chat_protocol_v2.md)
6. [System architecture](./puppyhub_system_context.png)
7. [Backend architecture](./puppyhub_backend_context.png)
