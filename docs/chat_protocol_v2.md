Chat protocol
===

This is the description of how you should interact with the live chat. Please also take a look at the server's [source code](https://gitlab.com/puppylovers1337/puppyhub/-/blob/master/backend/web/src/sockets/index.ts) to get a better understanding. The requirements are the following:
* to be authenticated
* use a SocketIO client

## Connection and synchronization protocol

1. `client`: connect to the server using authentication data `{auth: {token: "<jwt token>"}}`
2. `server`: send a `ready` event after your session is validated
3. `client`: wait for `ready` then send a `sync` event
4. `server`: responds with a `sync` event and data containing chat rooms of the user
5. `server`: send a `live` event after joining the client to all its chat rooms
6. `client`: wait for `sync` then store its chat rooms
7. `client`: wait for `live` event, after that you are able to send messages

## Live chat protocol

Messaging:
1. `client`: send a `message` event when you want to send a message to a chat room
2. `client`: receive `message` events from the server and store them (you will receive a `message` event even for your own messages, save them only after receiving it from the server)

New rooms:
1. `client`: receive `new_room` event when a new match was created and the client joined a new room, you should store it locally

## Message history

For accessing the message history you should take a look at backend API.
