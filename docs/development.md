Development guide
===

Below you can find which are the requirements and the commands you need to run to start PuppyHub.

## Install requirements
0. A `*nix` system is preferred (like `GNU/Linux`, `MacOS`, etc) but not mandatory.
1. We recommend you to use `VSCode` for development as it has great integration with technologies used. You can use any editor you like.
2. Install `docker` and `docker-compose`.
3. Install `node` and `npm`. (at the moment we use `15.11.0` for the official build, but any `stable` version is alright. We recommend to use `nvm` to manage your node versions)
4. Install `flutter`, `android-sdk` (for deploying on mobile) and `Google Chrome` (for deploying on web).
5. Run `npm install` inside `backend/web`.
6. Run `flutter pub get` inside `frontend/`.

## Running the project
1. To start the backend you simply need to go in `backend/` and run `./scripts/dev.sh`. If you are using Windows you can use `./scripts/dev.bat`. Now backend API should be available at `localhost:3000`.
2. To start the frontend application go to `frontend/` and run `flutter run -d <device>`. You device could be `chrome`, your phone or an emulator. If you are using `VSCode` you can install a `Flutter` extension and run it from the GUI.

**NB**: If you are running the frontend on your phone you have to have it connected to the same network as the machine where you are running the API. Also, you probably need to change the API connection string from `localhost:3000` to `<your lan ip eg. 192.168.x.x>:3000` in this [common/constants.dart](../frontend/lib/common/constants.dart).
