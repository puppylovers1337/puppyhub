# PuppyHub

The best place for your dog to find his/her happiness.

0. [Demooo](https://www.youtube.com/watch?v=E69jwkRCV6c) - video cu demo-ul si prezentarea aplicatiei
1. [Development process report](./docs/report.md) - raport pentru cursul de MDS
2. [Technical documentation](./docs/) - how to run the project, API documentation and other geek stuff
3. [Issue boards](https://gitlab.com/puppylovers1337/puppyhub/-/boards) - what we are doing now
4. [Milestones](https://gitlab.com/puppylovers1337/puppyhub/-/milestones) - what we are aiming for
