import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/models/state/credential_state.dart';
import 'package:puppy_hub/common/theme.dart';
import 'package:puppy_hub/models/state/message_state.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/util/route_generator.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (_) => CredentialState(),
      child: Consumer<CredentialState>(
        builder: (context, manager, child) {
          var materialApp = MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'PuppyHub',
            theme: new BaseTheme().theme,
            initialRoute: '/',
            onGenerateRoute: RouteGenerator(manager.hasToken).generateRoute,
          );

          if (manager.hasToken) {
            return MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (_) => UserState()),
                ChangeNotifierProvider(create: (_) => MessagesState()),
              ],
              child: materialApp,
            );
          } else {
            return materialApp;
          }
        },
      ),
    );
  }
}
