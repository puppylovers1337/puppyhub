class Profile {
  String id;
  String headline;
  String description;
  String image;

  Profile({
    this.id,
    this.headline,
    this.description,
    this.image,
  });

  factory Profile.fromJson(Map<String, dynamic> json) {
    return Profile(
      id: json['_id'] as String,
      headline: json['headline'] as String,
      description: json['description'] as String,
      image: json['image'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'headline': headline,
      'description': description,
    };
  }
}
