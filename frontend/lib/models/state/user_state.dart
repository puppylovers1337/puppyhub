import 'dart:typed_data';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/models/dog.dart';
import 'package:puppy_hub/models/user.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:puppy_hub/util/converter.dart';
import 'package:puppy_hub/util/image.dart' as ImageHelper;

const NO_PROFILE_IMAGE = '';

class UserState with ChangeNotifier {
  User _user;
  Map<String, Dog> _dogs;

  UserState() {
    _dogs = Map();
    _loadUser();
    _loadDogs();
  }

  User get user => _user;

  Iterable<Dog> getDogs() {
    return _dogs.values;
  }

  Dog getDog(String id) {
    if (!_dogs.containsKey(id)) return null;

    return _dogs[id];
  }

  void _setDog(Dog dog) {
    _dogs[dog.id] = dog;
  }

  void _deleteDog(String id) {
    if (!_dogs.containsKey(id)) return null;

    _dogs.remove(id);
    notifyListeners();
  }

  void _addDog(Dog dog) {
    _setDog(dog);
    notifyListeners();
  }

  // TODO: better way to update ?
  void _editDog(String id, Map<String, dynamic> data) {
    Dog dog = _dogs[id];
    data.keys.forEach((key) {
      var value = data[key];
      switch (key) {
        case 'name':
          if (dog.name != value) dog.name = value;
          break;
        case 'sex':
          if (EnumToString.convertToString(dog.gender) != value)
            dog.gender = Converter.stringToGender(value);
          break;
        case 'breed':
          if (dog.breed != value) dog.breed = value;
          break;
        case 'description':
          if (dog.description != value) dog.description = value;
      }
    });
    _dogs[id] = dog;
    notifyListeners();
  }

  void _addDogImage(String id, String fileName) {
    _dogs[id].images.add(fileName);
    notifyListeners();
  }

  Future<void> _loadUser() async {
    var userJson = await Api.get('users/me');
    _user = new User.fromJson(userJson);
    notifyListeners();
  }

  Future<void> _loadDogs() async {
    var dogs = await Api.get("users/me/dogs");
    dogs.forEach((dog) => _setDog(Dog.fromJson(dog)));
    notifyListeners();
  }

  ImageProvider getDogImage(String id) {
    // dogId could be null if function is called on create dog profile form
    return !_dogs.containsKey(id) || _dogs[id].images.isEmpty
        ? Image.network(kDefaultImageUrl).image
        : Image.network(kApiBaseUrl + "images/" + _dogs[id].images.last).image;
  }

  ImageProvider getProfileImage() {
    return _user.profile.image == NO_PROFILE_IMAGE
        ? Image.network(kDefaultImageUrl).image
        : Image.network(kApiBaseUrl + "images/" + _user.profile.image).image;
  }

  ImageProvider getMatchDogImage(Dog dog) {
    return dog.images.isEmpty
        ? Image.network(kDefaultImageUrl).image
        : Image.network(kApiBaseUrl + "images/" + dog.images.last).image;
  }

  Future<Dog> createDog(dynamic data) async {
    var dogJson = await Api.post("dogs/", data);
    Dog dog = Dog.fromJson(dogJson);
    _addDog(dog);
    return dog;
  }

  Future<void> _deleteDogImages(String id) async {
    _dogs[id].images.forEach((file) async {
      await Api.delete('dogs/$id/images/$file');
    });
  }

  Future<void> editDog(String id, dynamic data) async {
    var response = await Api.put('dogs/$id', data);
    if (response['ok'] == 1) {
      if (response['nModified'] > 0) _editDog(id, data);
    }
    // TODO: properly handle errors
  }

  Future<void> deleteDog(String id) async {
    await _deleteDogImages(id);
    await Api.delete("dogs/$id");
    _deleteDog(id);
  }

  Future<void> uploadDogImage(String dogId, Uint8List imageBytes) async {
    var formData = ImageHelper.getFormData(imageBytes);
    var fileName = await Api.put("dogs/$dogId/images", formData);

    _addDogImage(dogId, fileName['filenameWithExtension']);
  }

  Future<void> uploadProfileImage(Uint8List imageBytes) async {
    var formData = ImageHelper.getFormData(imageBytes);
    var fileName = await Api.put("users/me/image", formData);

    _user.profile.image = fileName['filenameWithExtension'];
    notifyListeners();
  }

  Future<void> editProfile(dynamic data) async {
    var response = await Api.put('users/me', data);
    if (response['ok'] == 1) {
      if (response['nModified'] > 0) _editProfile(data);
    }
    // TODO: properly handle errors
  }

  void _editProfile(Map<String, dynamic> data) {
    data['profile']['image'] = _user.profile.image;
    _user = User.fromJson(data);
    notifyListeners();
  }
}
