import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/models/chat/chat_room_view.dart';
import 'package:puppy_hub/models/chat/message.dart';
import 'package:puppy_hub/models/chat/room.dart';
import 'dart:collection';

import 'package:puppy_hub/networking/api/api.dart';

class MessagesState with ChangeNotifier {
  final int kDefaultMessageCount = 10;
  Map<String, ChatRoom> _rooms;
  Map<String, DoubleLinkedQueue<ChatMessage>> _messages;
  Map<String, bool> _isFetching;
  Map<String, bool> _prefechedBatch;

  MessagesState() {
    _rooms = Map();
    _messages = Map();
    _isFetching = Map();
    _prefechedBatch = Map();
  }

  Iterable<ChatRoom> getChatRooms() {
    return _rooms.values;
  }

  ChatRoom getChatRoom(String id) {
    if (_rooms.containsKey(id)) {
      return _rooms[id];
    }
    return null;
  }

  Iterable<ChatMessage> getMessagesForRoom(String roomId) {
    return _messages[roomId];
  }

  Iterable<ChatRoomView> getChatRoomViews() {
    return getChatRooms().map((room) => ChatRoomView(
        room,
        _messages[room.id].isEmpty
            ? ChatMessage(
                id: '',
                userId: '',
                roomId: room.id,
                content: 'Start to chat.',
                createdAt: room.lastUpdated)
            : _messages[room.id].first));
  }

  Future<void> loadRooms(Iterable<ChatRoom> rooms) async {
    rooms.forEach((element) => _setRoom(element));
    await Future.wait(
        rooms.map((element) => _prefetchFirstMessage(element.id)));
    notifyListeners();
  }

  void saveMessage(ChatMessage message) {
    if (!_rooms.containsKey(message.roomId)) return;
    _rooms[message.roomId].lastUpdated = message.createdAt;
    _messages[message.roomId].addFirst(message);
    notifyListeners();
  }

  void prefetch(String roomId) {
    if (_rooms.containsKey(roomId)) {
      _prefetchFirstBatch(roomId);
    }
  }

  void fetch(String roomId) {
    if (_rooms.containsKey(roomId)) {
      _fetchMessages(roomId, kDefaultMessageCount);
    }
  }

  String _getImageFromRoom(String roomId, String userId) {
    if (_rooms.containsKey(roomId) &&
        _rooms[roomId].participantsById.containsKey(userId)) {
      return _rooms[roomId].participantsById[userId].image;
    }
    return null;
  }

  NetworkImage getImageFromRoom(String roomId, String userId) {
    var imageName = _getImageFromRoom(roomId, userId);
    if (imageName == null) {
      return NetworkImage(kDefaultImageUrl);
    }
    return NetworkImage(kApiBaseUrl + "images/" + imageName);
  }

  void _setRoom(ChatRoom room) {
    _rooms[room.id] = room;
    _messages[room.id] = DoubleLinkedQueue();
    _isFetching[room.id] = false;
    _prefechedBatch[room.id] = false;
  }

  void setRoom(ChatRoom room) {
    _setRoom(room);
    notifyListeners();
  }

  Future<void> _prefetchFirstMessage(String roomId) async {
    await _fetchMessages(roomId, 1);
  }

  Future<void> _prefetchFirstBatch(String roomId) async {
    if (_prefechedBatch[roomId]) return;
    _prefechedBatch[roomId] = true;
    await _fetchMessages(roomId, kDefaultMessageCount);
  }

  void _addHistoricMessages(String roomId, List<ChatMessage> newMessages) {
    _messages[roomId].addAll(newMessages);
    notifyListeners();
  }

  Future<void> _fetchMessages(String roomId, int count) async {
    if (_isFetching[roomId]) return;
    _isFetching[roomId] = true;

    var date = _messages[roomId].isEmpty
        ? ""
        : ("&date=" + _messages[roomId].last.createdAt.toIso8601String());
    var data = await Api.get(
        "chat/messages/" + roomId + "?count=" + count.toString() + date);

    var newMessages =
        List<ChatMessage>.from(data.map((elem) => ChatMessage.fromJson(elem)));
    _addHistoricMessages(roomId, newMessages);

    _isFetching[roomId] = false;
  }
}
