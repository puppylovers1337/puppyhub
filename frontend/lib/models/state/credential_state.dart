import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter/foundation.dart' show ChangeNotifier, kIsWeb;
import 'package:puppy_hub/networking/api/api.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CredentialState with ChangeNotifier {
  static final FlutterSecureStorage storage =
      kIsWeb ? null : FlutterSecureStorage();

  bool _hasToken;

  CredentialState() {
    _hasToken = false;
  }

  bool get hasToken => _hasToken;

  set hasToken(bool value) {
    _hasToken = value;
    notifyListeners();
  }

  Future<String> getToken() async {
    if (kIsWeb) {
      var inst = await SharedPreferences.getInstance();
      return inst.getString("token");
    }
    return storage.read(key: "token");
  }

  Future<void> setToken(String token) async {
    if (kIsWeb) {
      var inst = await SharedPreferences.getInstance();
      inst.setString("token", token);
    } else {
      storage.write(key: "token", value: token);
    }

    Api.refreshToken(token);

    this.hasToken = true;
  }

  Future<void> clear() async {
    if (kIsWeb) {
      var inst = await SharedPreferences.getInstance();
      inst.clear();
    } else {
      storage.deleteAll();
    }

    this.hasToken = false;
  }
}
