import 'package:puppy_hub/models/chat/message.dart';
import 'package:puppy_hub/models/chat/room.dart';

class ChatRoomView {
  ChatRoom _chatRoom;
  ChatMessage _chatMessage;

  ChatRoom get chatRoom => this._chatRoom;

  set chatRoom(ChatRoom value) => this._chatRoom = value;

  ChatMessage get chatMessage => this._chatMessage;

  set chatMessage(value) => this._chatMessage = value;

  ChatRoomView(ChatRoom chatRoom, ChatMessage chatMessage) {
    _chatRoom = chatRoom;
    _chatMessage = chatMessage;
  }

  @override
  bool operator ==(Object other) {
    return (other is ChatRoomView) &&
        (other)._chatRoom.lastUpdated == _chatRoom.lastUpdated;
  }

  @override
  int get hashCode => _chatRoom.lastUpdated.hashCode;
}
