class ChatParticipant {
  String id;
  String userId;
  String dogId;
  String displayName;
  String image;

  ChatParticipant({
    this.id,
    this.userId,
    this.dogId,
    this.displayName,
    this.image,
  });

  factory ChatParticipant.fromJson(Map<String, dynamic> json) {
    return ChatParticipant(
      id: json['_id'] as String,
      userId: json['user_id'] as String,
      dogId: json['dog_id'] as String,
      displayName: json['displayName'] as String,
      image: json['image'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'user_id': userId,
      'dog_id': dogId,
      'displayName': displayName,
      'image': image,
    };
  }
}
