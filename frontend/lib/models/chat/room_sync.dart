import 'room.dart';
import 'message.dart';

class ChatRoomSync {
  ChatRoom chatRoom;
  List<ChatMessage> messages;

  ChatRoomSync({this.chatRoom, this.messages});

  factory ChatRoomSync.fromJson(Map<String, dynamic> json) {
    return ChatRoomSync(
      chatRoom: json['chatRoom'] == null
          ? null
          : ChatRoom.fromJson(json['chatRoom'] as Map<String, dynamic>),
      messages: (json['messages'] as List<dynamic>)
          ?.map((e) => e == null
              ? null
              : ChatMessage.fromJson(e as Map<String, dynamic>))
          ?.toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'chatRoom': chatRoom?.toJson(),
      'messages': messages?.map((e) => e?.toJson())?.toList(),
    };
  }
}
