class ChatMessage {
  String id;
  String userId;
  String roomId;
  String content;
  DateTime createdAt;

  ChatMessage({
    this.id,
    this.userId,
    this.roomId,
    this.content,
    this.createdAt,
  });

  factory ChatMessage.fromJson(Map<String, dynamic> json) {
    return ChatMessage(
      id: json['_id'] as String,
      userId: json['user_id'] as String,
      roomId: json['room_id'] as String,
      content: json['content'] as String,
      createdAt: DateTime.parse(json['createdAt']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'user_id': userId,
      'room_id': roomId,
      'content': content,
      'createdAt': createdAt.toIso8601String(),
    };
  }

  @override
  bool operator ==(Object other) {
    return (other is ChatMessage) && other.createdAt == this.createdAt;
  }

  @override
  int get hashCode => this.createdAt.hashCode;
}
