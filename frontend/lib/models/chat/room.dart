import 'participant.dart';

class ChatRoom {
  String id;
  DateTime lastUpdated;
  List<ChatParticipant> participants;
  Map<String, ChatParticipant> participantsById;

  ChatRoom({
    this.id,
    this.lastUpdated,
    this.participants,
  }) {
    participantsById = Map();
    participants.forEach((element) {
      participantsById[element.userId] = element;
    });
  }

  factory ChatRoom.fromJson(Map<String, dynamic> json) {
    return ChatRoom(
      id: json['_id'] as String,
      lastUpdated: DateTime.parse(json['lastUpdated']),
      participants: (json['participants'] as List<dynamic>)
          ?.map((e) => e == null
              ? null
              : ChatParticipant.fromJson(e as Map<String, dynamic>))
          ?.toList(),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'lastUpdated': lastUpdated.toIso8601String(),
      'participants': participants?.map((e) => e?.toJson())?.toList(),
    };
  }
}
