import 'profile.dart';

class User {
  String role;
  String id;
  String username;
  String email;
  String password;
  Profile profile;

  User({
    this.role,
    this.id,
    this.username,
    this.email,
    this.password,
    this.profile,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      role: json['role'] as String,
      id: json['_id'] as String,
      username: json['username'] as String,
      email: json['email'] as String,
      password: json['password'] as String,
      profile: json['profile'] == null
          ? null
          : Profile.fromJson(json['profile'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'role': role,
      '_id': id,
      'username': username,
      'email': email,
      'password': password,
      'profile': profile?.toJson(),
    };
  }
}
