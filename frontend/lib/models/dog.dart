import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:puppy_hub/util/converter.dart';

import 'location.dart';

enum Gender { male, female }

class Dog {
  List<String> images;
  String id;
  String userId;
  String name;
  Gender gender;
  String breed;
  String description;
  Location location;

  Dog({
    this.images,
    this.id,
    this.userId,
    this.name,
    this.gender,
    this.breed,
    this.description,
    this.location,
  });

  @override
  String toString() {
    return '''DogWithLocation(
      images: $images, 
      id: $id, 
      userId: $userId, 
      name: $name, 
      gender: $gender, 
      breed: $breed, 
      description: $description, 
      location: $location)''';
  }

  factory Dog.fromJson(Map<String, dynamic> json) {
    return Dog(
      images: json['images'] != null ? List<String>.from(json['images']) : null,
      id: json['_id'] as String,
      userId: json['user_id'] as String,
      name: json['name'] as String,
      gender: Converter.stringToGender(json['sex']),
      breed: json['breed'] as String,
      description: json['description'] as String,
      location: json['location'] == null
          ? null
          : Location.fromJson(json['location'] as Map<String, dynamic>),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'images': images,
      '_id': id,
      'user_id': userId,
      'name': name,
      'sex': EnumToString.convertToString(gender),
      'breed': breed,
      'description': description,
      'location': location?.toJson(),
    };
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Dog &&
        listEquals(other.images, images) &&
        other.id == id &&
        other.userId == userId &&
        other.name == name &&
        other.gender == gender &&
        other.breed == breed &&
        other.description == description &&
        other.location == location;
  }

  @override
  int get hashCode {
    return hashValues(
      images,
      id,
      userId,
      name,
      gender,
      breed,
      description,
      location,
    );
  }
}
