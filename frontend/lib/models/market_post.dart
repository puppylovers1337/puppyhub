import 'dart:convert';

MarketPost marketPostFromJson(String str) =>
    MarketPost.fromJson(json.decode(str));

String marketPostToJson(MarketPost data) => json.encode(data.toJson());

enum PostType { sell, babysit }

class MarketPost {
  MarketPost({
    this.title,
    this.description,
    this.price,
    this.type,
    this.images,
    this.id,
    this.userId,
    this.location,
    this.email,
    this.phoneNumber,
    this.createdAt,
  });

  String title;
  String description;
  String price;
  PostType type;
  List<dynamic> images;
  String id;
  String userId;
  String location;
  String email;
  String phoneNumber;
  DateTime createdAt;

  factory MarketPost.fromJson(Map<String, dynamic> json) => MarketPost(
      title: json["title"],
      description: json["description"],
      price: json["price"],
      type: json["type"] == "SELL" ? PostType.sell : PostType.babysit,
      images: List<dynamic>.from(json["images"].map((x) => x)),
      id: json["_id"],
      userId: json["user_id"],
      location: json["location"],
      email: json["email"],
      phoneNumber: json["phoneNumber"],
      createdAt: DateTime.parse(json['createdAt']));

  Map<String, dynamic> toJson() => {
        "title": title,
        "description": description,
        "price": price,
        "type": type == PostType.sell ? "SELL" : "BABYSIT",
        "images": List<dynamic>.from(images.map((x) => x)),
        "_id": id,
        "user_id": userId,
        "location": location,
        "email": email,
        "phoneNumber": phoneNumber,
        'createdAt': createdAt.toIso8601String(),
      };
}
