enum InterestType { breeding, walking }
enum Gender { male, female }

class MatchFilters {
  InterestType interestedIn = InterestType.breeding;
  List<String> breeds = [];
  int range = 10;

  Map<String, dynamic> toMap() {
    return {
      'interestedIn': interestedIn,
      'breeds': breeds,
      'distance': range,
    };
  }

  @override
  String toString() {
    return toMap().toString();
  }
}
