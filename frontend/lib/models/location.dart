import 'dart:ui';

import 'package:flutter/foundation.dart';

class Location {
  String type;
  List<int> coordinates;

  Location({this.type, this.coordinates});

  @override
  String toString() {
    return 'Location(type: $type, coordinates: $coordinates)';
  }

  factory Location.fromJson(Map<String, dynamic> json) {
    return Location(
      type: json['type'] as String,
      coordinates: List<int>.from(json['coordinates']),
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'type': type,
      'coordinates': coordinates,
    };
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Location &&
        other.type == type &&
        listEquals(other.coordinates, coordinates);
  }

  @override
  int get hashCode => hashValues(type, coordinates);
}
