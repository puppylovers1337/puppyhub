import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xff57419D);
const kPrimaryColorLight = Color(0xffb6a8ed);
const kApiBaseUrl = 'http://localhost:3000/';
const kSocketBaseUrl = 'ws://localhost:3000/';
const kDefaultImageUrl =
    'https://avatars.githubusercontent.com/u/50520077?s=400&u=92cf194a6844b8da4cde82c8838af9bbc967a152&v=4';
