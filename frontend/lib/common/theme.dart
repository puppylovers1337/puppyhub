import 'package:flutter/material.dart';
import 'package:puppy_hub/common/constants.dart';

class BaseTheme {
  ThemeData get theme => ThemeData(
        primaryColor: kPrimaryColor,
        primaryColorLight: kPrimaryColorLight,
        fontFamily: 'Roboto',
        textTheme: TextTheme(
          headline6: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold,
          ),
        ),
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Colors.white,
      );
}
