import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/models/chat/message.dart';
import 'package:puppy_hub/models/chat/room.dart';
import 'package:puppy_hub/models/state/message_state.dart';
import 'package:socket_io_client/socket_io_client.dart';

class SocketService {
  Socket _socket;
  MessagesState _messagesState;
  bool _isLive = false;

  // TODO: sometimes the chat connection fails. fix and test it

  SocketService(String token, MessagesState messagesState) {
    _socket = io(
        kSocketBaseUrl,
        OptionBuilder()
            .disableAutoConnect()
            .setTransports(['websocket']).setAuth({"token": token}).build());
    _messagesState = messagesState;

    _socket.onConnect(handleConnect);
    _socket.on('ready', handleReady);
    _socket.on('sync', handleSync);
    _socket.on('live', handleLive);
    _socket.on('message', handleMessage);
    _socket.on('new_room', handleNewRoom);
    _socket.onDisconnect(handleDisconnect);
  }

  dynamic handleConnect(dynamic data) {}

  dynamic handleReady(dynamic data) {
    _socket.emit('sync');
  }

  dynamic handleSync(dynamic data) {
    var rooms = List<ChatRoom>.from(
        data.map((roomJson) => ChatRoom.fromJson(roomJson)));
    _messagesState.loadRooms(rooms);
  }

  dynamic handleLive(dynamic data) {
    _isLive = true;
  }

  // TODO: handle errors from server
  dynamic handleMessage(dynamic data) {
    var message = ChatMessage.fromJson(data);

    _messagesState.saveMessage(message);
  }

  dynamic handleNewRoom(dynamic data) {
    var room = ChatRoom.fromJson(data);
    _messagesState.setRoom(room);
  }

  dynamic handleDisconnect(dynamic data) {}

  void sendMessage(String roomId, String content) {
    if (!_isLive) return;

    _socket.emit('message', [
      {"roomId": roomId, "content": content}
    ]);
  }

  void connect() {
    _socket.connect();
  }

  void dispose() {
    _socket.dispose();
  }
}
