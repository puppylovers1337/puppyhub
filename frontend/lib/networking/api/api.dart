import 'dart:io';

import 'package:dio/dio.dart';
import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/networking/api/custom_exception.dart';

class Api {
  static final Dio dio = Dio();

  static void refreshToken(String token) {
    dio.interceptors.clear();
    dio.interceptors.add(InterceptorsWrapper(onRequest: (options, request) {
      options.headers["Authorization"] = "Bearer " + token;
      return request.next(options);
    }));
  }

  static Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await dio.get(kApiBaseUrl + url);
      responseJson = response.data;
    } catch (e) {
      if (e is SocketException) handleSocketException(e);
      if (e is DioError) handleDioError(e);
    }
    return responseJson;
  }

  static Future<dynamic> post(String url, dynamic data) async {
    var responseJson;
    try {
      final response = await dio.post(kApiBaseUrl + url, data: data);
      responseJson = response.data;
    } catch (e) {
      if (e is SocketException) handleSocketException(e);
      if (e is DioError) handleDioError(e);
    }
    return responseJson;
  }

  static Future<dynamic> put(String url, dynamic data) async {
    var responseJson;
    try {
      final response = await dio.put(kApiBaseUrl + url, data: data);
      responseJson = response.data;
    } catch (e) {
      if (e is SocketException) handleSocketException(e);
      if (e is DioError) handleDioError(e);
    }
    return responseJson;
  }

  static Future<dynamic> delete(String url) async {
    var responseJson;
    try {
      final response = await dio.delete(kApiBaseUrl + url);
      responseJson = response.data;
    } catch (e) {
      if (e is SocketException) handleSocketException(e);
      if (e is DioError) handleDioError(e);
    }
    return responseJson;
  }

  static void handleDioError(DioError e) {
    if (e is DioError) {
      if (e.response == null) {
        throw Exception(e);
      }
      switch (e.response.statusCode) {
        case 400:
          throw BadRequestException(e.response.data);
        case 401:

        case 403:
          throw UnauthorisedException(e.response.data);
        case 500:

        default:
          throw FetchDataException(
              'Error occured while Communication with Server with StatusCode : ${e.response.statusCode}');
      }
    }
  }

  static void handleSocketException(SocketException e) {
    throw FetchDataException('No Internet connection');
  }
}
