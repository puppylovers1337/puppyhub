import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/models/state/credential_state.dart';
import 'package:puppy_hub/models/state/message_state.dart';
import 'package:puppy_hub/networking/socket/socket_service.dart';
import 'package:puppy_hub/screens/chat/chat_list/chat_list_screen.dart';
import 'package:puppy_hub/screens/contest/contest_list/contest_list_screen.dart';
import 'package:puppy_hub/screens/map/map_screen.dart';
import 'package:puppy_hub/screens/market/market_list/market_list_screen.dart';
import 'package:puppy_hub/screens/user/home/home_screen.dart';
import 'package:puppy_hub/util/locator.dart';

class MainScreen extends StatefulWidget {
  @override
  _StatefulWidgetState createState() => _StatefulWidgetState();
}

class _StatefulWidgetState extends State<MainScreen> {
  int _selectedIndex = 2;
  PageStorageBucket _bucket = PageStorageBucket();
  List<Widget> pages = [
    MarketListScreen(key: PageStorageKey<String>("Market")),
    ContestListScreen(key: PageStorageKey<String>("Contest")),
    HomeScreen(key: PageStorageKey<String>("Home")),
    ChatListScreen(key: PageStorageKey<String>("Chat")),
    MapScreen(key: PageStorageKey<String>("Map")),
  ];

  void _onItemTapped(int index) {
    setState(() => _selectedIndex = index);
  }

  Future<void> _socketConnect() async {
    var token = await context.read<CredentialState>().getToken();
    getIt.registerSingleton<SocketService>(
        SocketService(token, context.read<MessagesState>()));
    getIt<SocketService>().connect();
  }

  @override
  void initState() {
    super.initState();
    _socketConnect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageStorage(
        child: pages[_selectedIndex],
        bucket: _bucket,
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Theme.of(context).primaryColor,
        currentIndex: _selectedIndex,
        onTap: _onItemTapped,
        unselectedItemColor: Theme.of(context).primaryColor.withOpacity(.60),
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            label: 'Market',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.star),
            label: 'Contest',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.chat),
            label: 'Chat',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.map),
            label: 'Map',
          ),
        ],
      ),
    );
  }
}
