import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class FancyMarker extends StatefulWidget {
  final String text;

  FancyMarker({Key key, this.text}) : super(key: key);

  @override
  _FancyMarkerState createState() => _FancyMarkerState();
}

class _FancyMarkerState extends State<FancyMarker> {
  bool isActive = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Opacity(
          opacity: isActive ? 1 : 0,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Theme.of(context).primaryColorLight,
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
              child: Text(
                widget.text,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ),
        SizedBox(height: 10),
        GestureDetector(
          child: Icon(
            Icons.location_pin,
            color: Colors.purple,
          ),
          onTap: () => {
            setState(() {
              isActive = !isActive;
            })
          },
        ),
      ],
    );
  }
}

Marker buildMarker(LatLng pos, String text) {
  return Marker(
    width: 100,
    height: 100,
    point: pos,
    builder: (ctx) => FancyMarker(text: text),
  );
}

var markers = [
  [LatLng(44.428391, 26.090617), "Happy Dogs Adventure Park"],
  [LatLng(44.467228, 26.052295), "TerraVet"],
  [LatLng(44.448855, 26.123472), "Dr. Doggo"],
  [LatLng(44.426753, 26.113327), "OnlyDogs"],
];

class MapScreen extends StatelessWidget {
  const MapScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FlutterMap(
      options: MapOptions(
        center: LatLng(44.439663, 26.096306),
        zoom: 13.0,
      ),
      layers: [
        TileLayerOptions(
            urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            subdomains: ['a', 'b', 'c']),
        MarkerLayerOptions(
          markers: markers.map((e) => buildMarker(e[0], e[1])).toList(),
        ),
      ],
    );
  }
}
