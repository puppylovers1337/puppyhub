import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';

import 'components/avatar.dart';
import 'components/body.dart';

class ChatListScreen extends StatelessWidget {
  const ChatListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Chat',
        leadingWidget: Avatar(
          onPressed: () {},
        ),
      ),
      body: Body(),
    );
  }
}
