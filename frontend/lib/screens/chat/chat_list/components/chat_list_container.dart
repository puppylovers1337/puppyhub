import 'package:flutter/material.dart';

import 'avatar.dart';
import 'chat_list_card.dart';

class ChatListContainer extends StatelessWidget {
  final String chatDogName, userDogName, lastMessage, date;
  final NetworkImage image;
  final Function onTap;

  ChatListContainer({
    Key key,
    @required this.chatDogName,
    @required this.userDogName,
    this.lastMessage,
    this.date,
    @required this.image,
    @required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: this.onTap,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Container(
          child: Row(
            children: <Widget>[
              Avatar(image: this.image),
              Expanded(
                child: ChatListCard(
                  chatDogName: chatDogName,
                  userDogName: userDogName,
                  lastMessage: lastMessage,
                  date: date,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
