import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/models/chat/chat_room_view.dart';
import 'package:puppy_hub/models/state/message_state.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/util/converter.dart';

import 'chat_list_container.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String userId =
        context.select((UserState userProvider) => userProvider.user.id);

    Widget _chatListBuilder(
        BuildContext context, int index, List<ChatRoomView> data) {
      var chatRoom = data[index].chatRoom;
      var lastMessage = data[index].chatMessage;
      var participants = chatRoom.participants;
      var roomId = chatRoom.id;
      var date = Converter.toChatListFormat(lastMessage.createdAt);

      var myIndex = participants[0].userId == userId ? 0 : 1;
      var me = participants[myIndex];
      var other = participants[1 - myIndex];

      return ChatListContainer(
          chatDogName: other.displayName,
          userDogName: me.displayName,
          lastMessage: lastMessage.content,
          image: context
              .read<MessagesState>()
              .getImageFromRoom(roomId, other.userId),
          date: date,
          onTap: () {
            Navigator.of(context)
                .pushNamed("/chat/user", arguments: {'roomId': roomId});
          });
    }

    // TODO: find why this did not work with Selector<>
    // ANSWER: https://github.com/rrousselGit/provider/issues/560
    return Consumer<MessagesState>(builder: (context, data, child) {
      var roomsViews = data.getChatRoomViews().toList();
      if (roomsViews.length > 0) {
        return ListView.builder(
            itemCount: roomsViews.length,
            itemBuilder: (context, index) =>
                _chatListBuilder(context, index, roomsViews));
      }
      return Center(child: Text("No matches till now."));
    });
  }
}
