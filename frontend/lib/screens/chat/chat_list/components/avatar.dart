import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final NetworkImage image;
  final Function onPressed;
  final double radius;

  Avatar({
    Key key,
    //TODO: required
    this.image,
    this.onPressed,
    this.radius,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: onPressed,
        child: CircleAvatar(
          radius: radius != null ? radius : 40,
          backgroundImage: image,
          backgroundColor: Colors.transparent,
        ),
      ),
      decoration: new BoxDecoration(
        shape: BoxShape.circle,
        border: new Border.all(
          color: Theme.of(context).primaryColor,
          width: 2.0,
        ),
      ),
    );
  }
}
