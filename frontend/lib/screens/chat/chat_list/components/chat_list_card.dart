import 'package:flutter/material.dart';

class ChatListCard extends StatelessWidget {
  final String chatDogName, userDogName, lastMessage, date;

  ChatListCard({
    Key key,
    this.chatDogName,
    this.userDogName,
    this.lastMessage,
    this.date,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.only(left: 15),
      child: Container(
        decoration: new BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 2.0,
              spreadRadius: 0.0,
            )
          ],
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Padding(
          padding: EdgeInsets.all(15),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    chatDogName,
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(userDogName),
                ],
              ),
              SizedBox(height: size.height * 0.02),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(lastMessage),
              ),
              SizedBox(height: size.height * 0.02),
              Align(
                alignment: Alignment.bottomRight,
                child: Text(date),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
