import "package:flutter/material.dart";
import 'package:provider/provider.dart';
import 'package:puppy_hub/models/chat/message.dart';
import 'package:puppy_hub/models/state/message_state.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/screens/chat/chat_list/components/avatar.dart';
import 'package:puppy_hub/util/converter.dart';

import 'components/add_message.dart';
import 'components/chat_user_appbar.dart';
import 'components/message.dart';

class ChatUserScreen extends StatelessWidget {
  final String roomId;
  const ChatUserScreen({Key key, this.roomId}) : super(key: key);

  String getMessageType(String userId, ChatMessage message) {
    return message.userId == userId ? "sender" : "receiver";
  }

  Widget _messagesBuilder(BuildContext context, List<ChatMessage> messages,
      ScrollController scrollController) {
    var userId =
        context.select((UserState userProvider) => userProvider.user.id);

    if (messages.isEmpty) {
      return Center(child: Text("Start to chat."));
    }

    return ListView.builder(
      controller: scrollController,
      itemCount: messages.length,
      reverse: true,
      itemBuilder: (BuildContext context, int index) => Message(
        messageContent: messages[index].content,
        messageType: getMessageType(userId, messages[index]),
        time: Converter.toChatMessageFormat(messages[index].createdAt),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    context.read<MessagesState>().prefetch(roomId);

    var chatRoom = context.read<MessagesState>().getChatRoom(roomId);
    var userId = context.read<UserState>().user.id;
    var myIndex = chatRoom.participants[0].userId == userId ? 0 : 1;
    var me = chatRoom.participants[myIndex];
    var other = chatRoom.participants[1 - myIndex];

    var otherDogImage =
        context.read<MessagesState>().getImageFromRoom(roomId, other.userId);
    var meDogImage =
        context.read<MessagesState>().getImageFromRoom(roomId, me.userId);

    return Scaffold(
      appBar: ChatUserAppBar(
        chatDogName: other.displayName,
        chatDogAvatar: Avatar(radius: 20, image: otherDogImage),
        userDogAvatar: Avatar(radius: 20, image: meDogImage),
      ),
      body: Column(
        children: [
          Expanded(
            child: Selector<MessagesState, List<ChatMessage>>(
              selector: (context, messagesState) =>
                  messagesState.getMessagesForRoom(roomId).toList(),
              builder: (context, data, child) => DraggableScrollableSheet(
                  initialChildSize: 0.9,
                  minChildSize: 0.9,
                  builder: (context, scrollController) {
                    _scrollListener() {
                      if (scrollController.position.pixels ==
                          scrollController.position.maxScrollExtent) {
                        context.read<MessagesState>().fetch(roomId);
                      }
                    }

                    scrollController.addListener(_scrollListener);

                    return _messagesBuilder(context, data, scrollController);
                  }),
            ),
          ),
          Align(
            alignment: Alignment.bottomLeft,
            child: AddMessageBar(roomId: this.roomId),
          ),
        ],
      ),
    );
  }
}
