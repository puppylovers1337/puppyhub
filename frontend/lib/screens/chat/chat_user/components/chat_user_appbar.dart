import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ChatUserAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String chatDogName;
  final Widget userDogAvatar;
  final Widget chatDogAvatar;

  const ChatUserAppBar({
    Key key,
    this.chatDogName,
    this.userDogAvatar,
    this.chatDogAvatar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      automaticallyImplyLeading: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      title: Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: chatDogAvatar,
            ),
            Text(this.chatDogName),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: userDogAvatar,
            ),
          ],
        ),
      ),
      actions: [
        Icon(
          Icons.favorite,
          color: Colors.transparent,
        ),
        Icon(
          Icons.favorite,
          color: Colors.transparent,
        ),
      ],
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
