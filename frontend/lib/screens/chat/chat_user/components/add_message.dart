import 'package:flutter/material.dart';
import 'package:puppy_hub/networking/socket/socket_service.dart';
import 'package:puppy_hub/util/locator.dart';

class AddMessageBar extends StatefulWidget {
  final roomId;

  AddMessageBar({Key key, this.roomId}) : super(key: key);

  @override
  _AddMessageBarState createState() => _AddMessageBarState();
}

class _AddMessageBarState extends State<AddMessageBar> {
  final TextEditingController _messageController = TextEditingController();
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _sendMessage() {
    if (_messageController.text != '') {
      var roomId = widget.roomId;
      var content = _messageController.text;
      getIt<SocketService>().sendMessage(roomId, content);
      _messageController.clear();
      _focusNode.requestFocus();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.grey,
            blurRadius: 10.0,
          )
        ],
      ),
      padding: EdgeInsets.all(10),
      height: 60,
      width: double.infinity,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 15,
          ),
          Expanded(
            child: TextField(
              focusNode: _focusNode,
              controller: _messageController,
              decoration: InputDecoration(
                hintText: "Write message...",
                hintStyle: TextStyle(color: Colors.black54),
                border: InputBorder.none,
              ),
              onSubmitted: (_) => _sendMessage(),
            ),
          ),
          SizedBox(
            width: 12,
          ),
          FloatingActionButton(
            onPressed: _sendMessage,
            child: Icon(
              Icons.send,
              color: Theme.of(context).primaryColor,
              size: 18,
            ),
            backgroundColor: Colors.white,
            elevation: 5,
          ),
        ],
      ),
    );
  }
}
