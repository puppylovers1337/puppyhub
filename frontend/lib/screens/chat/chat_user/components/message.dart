import 'package:flutter/material.dart';

class Message extends StatelessWidget {
  final String messageContent, messageType, time;

  Message({Key key, this.messageContent, this.messageType, this.time})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(left: 14, right: 14, top: 10, bottom: 10),
      child: Align(
        alignment: (messageType == "receiver"
            ? Alignment.topLeft
            : Alignment.topRight),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: (messageType == "receiver"
                ? Colors.grey.shade200
                : Colors.grey.shade300),
          ),
          padding: EdgeInsets.only(
            left: 15,
            top: 15,
            right: 15,
            bottom: 5,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Text(
                messageContent,
                style: TextStyle(fontSize: 15),
              ),
              SizedBox(height: size.height * 0.005),
              Text(
                time,
                style: TextStyle(fontSize: 12),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
