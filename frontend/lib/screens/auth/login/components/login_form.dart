import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/components/rounded_input_field.dart';
import 'package:puppy_hub/components/rounded_password_field.dart';
import 'package:puppy_hub/models/state/credential_state.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:provider/provider.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String errorMssage;

  @override
  void dispose() {
    super.dispose();
    usernameController.dispose();
    passwordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Function submitForm = () {
      if (formKey.currentState.validate()) {
        dynamic data = {
          "username": usernameController.text,
          "password": passwordController.text,
        };

        Api.post('auth/login', data).then((response) async {
          await context.read<CredentialState>().setToken(response['token']);
        }).catchError((Object e) {
          setState(() {
            errorMssage = "User or password incorrect";
          });
          // TODO: properly handle error messages
        });
      }
    };

    return Form(
      key: formKey,
      child: Column(
        children: [
          if (errorMssage != null)
            Text(
              errorMssage,
              style: TextStyle(
                color: Colors.red,
              ),
            ),
          RoundedInputField(
            // TODO : better looking error messages
            validator: (value) =>
                value.isEmpty ? "Introduce your username" : null,
            hintText: "Username",
            icon: Icons.person,
            controller: usernameController,
          ),
          RoundedPasswordField(
            validator: (value) =>
                value.isEmpty ? "Introduce your password" : null,
            controller: passwordController,
          ),
          RoundedButton(
            text: "LOGIN",
            widthRatio: 0.7,
            press: submitForm,
          ),
        ],
      ),
    );
  }
}
