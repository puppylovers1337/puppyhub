import 'package:flutter/material.dart';
import 'package:puppy_hub/components/already_have_an_account_check.dart';
import 'background.dart';
import 'login_form.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "LOGIN",
              style: Theme.of(context).textTheme.headline6,
            ),
            CircleAvatar(
              radius: size.height * 0.15,
              backgroundImage: AssetImage('assets/images/logo.jpg'),
              backgroundColor: Colors.transparent,
            ),
            SizedBox(height: size.height * 0.02),
            LoginForm(),
            SizedBox(height: size.height * 0.03),
            AlreadyHaveAnAccountCheck(press: () {
              Navigator.of(context).pushReplacementNamed('/register');
            })
          ],
        ),
      ),
    );
  }
}
