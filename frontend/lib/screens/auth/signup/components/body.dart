import 'package:flutter/material.dart';
import 'package:puppy_hub/components/already_have_an_account_check.dart';
import 'background.dart';
import 'signup_form.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "SIGN UP",
              style: TextStyle(
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
            CircleAvatar(
              radius: size.height * 0.15,
              backgroundImage: AssetImage('assets/images/logo.jpg'),
              backgroundColor: Colors.transparent,
            ),
            SignUpForm(),
            SizedBox(
              height: size.height * 0.03,
            ),
            AlreadyHaveAnAccountCheck(
              login: false,
              press: () {
                Navigator.of(context).pushReplacementNamed('/login');
              },
            )
          ],
        ),
      ),
    );
  }
}
