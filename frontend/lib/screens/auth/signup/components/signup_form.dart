import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/components/rounded_input_field.dart';
import 'package:puppy_hub/components/rounded_password_field.dart';
import 'package:puppy_hub/models/state/credential_state.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/util/validator.dart';

class SignUpForm extends StatefulWidget {
  @override
  _SignUpFormState createState() => _SignUpFormState();
}

class _SignUpFormState extends State<SignUpForm> {
  final usernameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String errorMessage;

  @override
  Widget build(BuildContext context) {
    Function submitForm = () async {
      if (formKey.currentState.validate()) {
        try {
          dynamic data = {
            "username": usernameController.text,
            "email": emailController.text,
            "password": passwordController.text,
          };

          await Api.post('auth/register', data);

          dynamic user = {
            "username": data["username"],
            "password": data["password"],
          };

          var response = await Api.post('auth/login', user);
          await context.read<CredentialState>().setToken(response['token']);
        } catch (_) {
          // TODO: properly handle error messages
          // TODO : only if code 500 error
          setState(() {
            errorMessage = "You already have an account.";
          });
        }
      }
    };

    return Form(
      key: formKey,
      child: Column(
        children: [
          if (errorMessage != null)
            Text(
              errorMessage,
              style: TextStyle(
                color: Colors.red,
              ),
            ),
          RoundedInputField(
            hintText: "Username",
            icon: Icons.person,
            validator: Validator.username,
            controller: usernameController,
          ),
          RoundedInputField(
            hintText: "Your Email",
            icon: Icons.email,
            validator: Validator.email,
            controller: emailController,
          ),
          RoundedPasswordField(
            validator: Validator.password,
            controller: passwordController,
          ),
          RoundedButton(
            text: "Register",
            widthRatio: 0.7,
            press: submitForm,
          ),
        ],
      ),
    );
  }
}
