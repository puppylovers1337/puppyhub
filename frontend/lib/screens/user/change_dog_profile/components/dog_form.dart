import 'dart:typed_data';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/dog.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:enum_to_string/enum_to_string.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:puppy_hub/util/validator.dart';

import 'input_field.dart';
import 'upload_photo_section.dart';

class DogForm extends StatefulWidget {
  final String dogId;

  const DogForm({
    Key key,
    this.dogId,
  }) : super(key: key);

  @override
  _DogFormState createState() => _DogFormState();
}

enum FormType { create, edit }

class _DogFormState extends State<DogForm> {
  final _nameController = TextEditingController();
  final _descriptionController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String errorMessage;

  FormType formType;
  Gender _gender = Gender.male;
  Dog dog;
  Uint8List _imageBytes;
  List<String> availableBreeds = [];
  String _selectedBreed;

  void _loadBreeds() async {
    var breeds = List<String>.from(await Api.get('dogs/breeds'));
    setState(() {
      availableBreeds = breeds;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadBreeds();
    formType = widget.dogId == null ? FormType.create : FormType.edit;

    if (formType == FormType.edit) {
      dog = context.read<UserState>().getDog(widget.dogId);
      _gender = dog.gender;
      _nameController.text = dog?.name;
      _selectedBreed = dog?.breed;
      _descriptionController.text = dog?.description;
    }
  }

  @override
  void dispose() {
    super.dispose();
    _nameController.dispose();
    _descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Function submitForm = () async {
      if (formKey.currentState.validate()) {
        dynamic data = {
          "name": _nameController.text,
          "sex": EnumToString.convertToString(_gender),
          "breed": _selectedBreed,
          "description": _descriptionController.text,
          "location": {
            "type": "Point",
            // TODO: add geolocation
            "coordinates": [0, 0],
          }
        };

        if (formType == FormType.create) {
          dog = await context.read<UserState>().createDog(data);
        } else {
          await context.read<UserState>().editDog(dog.id, data);
        }

        if (_imageBytes != null) {
          await context.read<UserState>().uploadDogImage(dog.id, _imageBytes);
        }

        Navigator.of(context).pop();
      }
    };

    Function _setImageBytes = (Uint8List imageBytes) {
      setState(() {
        _imageBytes = imageBytes;
      });
    };

    return Form(
      key: formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (errorMessage != null)
              Text(
                errorMessage,
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            UploadPhotoSection(
              buttonText:
                  formType == FormType.edit ? "Change photo" : "Upload photo",
              fileBytes: _imageBytes,
              setImageBytes: _setImageBytes,
              dogId: dog?.id,
            ),
            InputField(
              leadingTitle: "Name",
              controller: _nameController,
              validator: Validator.dogName,
            ),
            Row(
              children: [
                Padding(
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 28,
                    right: 28,
                  ),
                  child: Text(
                    "Gender",
                    style: Theme.of(context).textTheme.bodyText2,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: 20,
                    left: 28,
                    right: 28,
                  ),
                  child: DropdownButton(
                    value: _gender,
                    items: [
                      DropdownMenuItem(
                        child: Text("Male"),
                        value: Gender.male,
                      ),
                      DropdownMenuItem(
                        child: Text("Female"),
                        value: Gender.female,
                      ),
                    ],
                    onChanged: (value) {
                      setState(() {
                        _gender = value;
                      });
                    },
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 30),
              child: DropdownSearch<String>(
                mode: Mode.MENU,
                showSelectedItem: true,
                items: this.availableBreeds,
                showSearchBox: true,
                hint: "Select dog breed",
                onChanged: (value) {
                  _selectedBreed = value;
                },
                validator: Validator.breed,
                selectedItem: _selectedBreed,
                autoValidateMode: AutovalidateMode.onUserInteraction,
              ),
            ),
            InputField(
              leadingTitle: "Description",
              controller: _descriptionController,
            ),
            SizedBox(height: 40),
            RoundedButton(
              text:
                  formType == FormType.create ? "Add Profile" : "Save Changes",
              press: submitForm,
              widthRatio: 0.8,
            ),
            SizedBox(height: 20)
          ],
        ),
      ),
    );
  }
}
