import "package:flutter/material.dart";
import 'package:puppy_hub/components/base_appbar.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';
import 'components/dog_form.dart';

class ChangeDogProfileScreen extends StatelessWidget {
  final String dogId;

  ChangeDogProfileScreen({
    Key key,
    this.dogId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _title = dogId == null ? "New Profile" : "Edit Profile";

    return Scaffold(
      appBar: BaseAppBar(
        title: _title,
        hasArrowBack: true,
        actionWidget: GestureDetector(
          onTap: () {},
          child: CircleAvatar(
            radius: 21,
            backgroundColor: Theme.of(context).primaryColorLight,
            child: CircleAvatar(
              radius: 20,
              backgroundImage: context.watch<UserState>().getProfileImage(),
            ),
          ),
        ),
      ),
      body: DogForm(dogId: this.dogId),
    );
  }
}
