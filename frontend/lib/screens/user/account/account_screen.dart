import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';

import 'components/body.dart';

class AccountScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: BaseAppBar(
        title: 'My Account',
        hasArrowBack: true,
      ),
      body: Body(),
    );
  }
}
