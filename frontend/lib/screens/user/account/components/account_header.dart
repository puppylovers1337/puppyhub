import 'package:flutter/material.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';

class AccountHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    ImageProvider profileImage = context
        .select<UserState, ImageProvider>((state) => state.getProfileImage());
    String username =
        context.select<UserState, String>((state) => state.user.username);
    String headline = context
        .select<UserState, String>((state) => state.user.profile.headline);

    return Container(
      height: size.height * 0.35,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [Colors.white, Theme.of(context).primaryColor],
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 15, bottom: 15),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircleAvatar(
                backgroundImage: profileImage,
                radius: 50.0,
              ),
              SizedBox(height: size.height * 0.02),
              Text(
                username,
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: size.height * 0.01),
              Text(
                headline,
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: size.height * 0.02),
            ],
          ),
        ),
      ),
    );
  }
}
