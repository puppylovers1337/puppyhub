import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puppy_hub/models/state/credential_state.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/models/user.dart';
import 'package:puppy_hub/networking/socket/socket_service.dart';
import 'package:puppy_hub/util/locator.dart';
import 'package:provider/provider.dart';

import 'account_field.dart';
import 'account_header.dart';
import 'rounded_button.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = context.select<UserState, User>((userState) => userState.user);

    return SingleChildScrollView(
      child: Column(
        children: [
          AccountHeader(),
          Column(
            children: [
              AccountField(
                  icon:
                      Icon(Icons.email, color: Theme.of(context).primaryColor),
                  title: user.email),
              Divider(color: Theme.of(context).primaryColor, indent: 16.0),
              AccountField(
                icon: Icon(
                  Icons.description,
                  color: Theme.of(context).primaryColor,
                ),
                title: user.profile.description,
              ),
              Divider(color: Theme.of(context).primaryColor, indent: 16.0),
              SizedBox(height: 20),
              RoundedButton(
                text: "Edit profile",
                callback: () {
                  Navigator.of(context).pushNamed('/account/edit');
                },
              ),
              SizedBox(height: 20),
              RoundedButton(
                text: "Logout",
                callback: () {
                  getIt.unregister<SocketService>(
                      disposingFunction: (socketService) =>
                          socketService.dispose());
                  context.read<CredentialState>().clear();
                },
              ),
              SizedBox(height: 20),
            ],
          )
        ],
      ),
    );
  }
}
