import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AccountField extends StatelessWidget {
  final Icon icon;
  final String title;
  final String subtitle;

  const AccountField({
    Key key,
    this.icon,
    this.title,
    this.subtitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: ListTile(
        leading: this.icon,
        title: new Text(this.title),
        subtitle: this.subtitle != null ? new Text(this.subtitle) : null,
      ),
    );
  }
}
