import "package:flutter/material.dart";
import 'package:puppy_hub/components/base_appbar.dart';

import 'components/body.dart';

class AccountEditScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: "Edit Account",
        hasArrowBack: true,
      ),
      body: Body(),
    );
  }
}
