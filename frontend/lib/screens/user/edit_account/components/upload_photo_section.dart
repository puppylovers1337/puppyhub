import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/util/image.dart' as ImageHelper;
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';

class UploadPhotoSection extends StatefulWidget {
  final String buttonText;
  final Function(Uint8List) setImageBytes;
  final Uint8List imageBytes;

  const UploadPhotoSection({
    Key key,
    this.buttonText,
    this.setImageBytes,
    this.imageBytes,
  }) : super(key: key);

  @override
  _UploadPhotoSectionState createState() => _UploadPhotoSectionState();
}

class _UploadPhotoSectionState extends State<UploadPhotoSection> {
  Image getImage() {
    return widget.imageBytes == null
        ? Image(
            image: context.read<UserState>().getProfileImage(),
            width: 100,
            height: 100,
            fit: BoxFit.fitWidth,
          )
        : Image.memory(
            widget.imageBytes,
            width: 100,
            height: 100,
            fit: BoxFit.fitWidth,
          );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 130,
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: 30.0),
              child: CircleAvatar(
                radius: 44,
                backgroundColor: Theme.of(context).primaryColorLight,
                child: CircleAvatar(
                  radius: 44,
                  backgroundColor: Theme.of(context).primaryColorLight,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: getImage(),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 30.0),
              child: RoundedButton(
                text: widget.buttonText,
                press: () async {
                  var imageBytes = await ImageHelper.readImageAsBytes();
                  if (imageBytes == null) return;
                  widget.setImageBytes(imageBytes);
                },
                widthRatio: 0.5,
              ),
            ),
          ],
        ));
  }
}
