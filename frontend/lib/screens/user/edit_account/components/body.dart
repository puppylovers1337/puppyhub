import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:puppy_hub/models/user.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';

import 'input_field.dart';
import 'upload_photo_section.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  int _value = 1;
  Uint8List _imageBytes;

  final formKey = GlobalKey<FormState>();
  final _usernameController = TextEditingController();
  final _headlineController = TextEditingController();
  final _emailController = TextEditingController();
  final _descriptionController = TextEditingController();

  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _usernameController.dispose();
    _headlineController.dispose();
    _emailController.dispose();
    _descriptionController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    var user = context.select<UserState, User>((userState) => userState.user);
    _usernameController.text = user.username;
    _headlineController.text = user.profile.headline;
    _emailController.text = user.email;
    _descriptionController.text = user.profile.description;

    Function submitForm = () async {
      if (formKey.currentState.validate()) {
        dynamic data = {
          "username": _usernameController.text,
          "email": _emailController.text,
          "profile": {
            "headline": _headlineController.text,
            "description": _descriptionController.text
          }
        };

        await context.read<UserState>().editProfile(data);
      }

      if (_imageBytes != null) {
        await context.read<UserState>().uploadProfileImage(_imageBytes);
      }

      Navigator.of(context).pop();
    };

    Function _setImageBytes = (Uint8List imageBytes) {
      setState(() {
        _imageBytes = imageBytes;
      });
    };

    return Form(
      key: formKey,
      child: Row(
        children: [
          SizedBox(
            width: size.width * 0.02,
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  UploadPhotoSection(
                    buttonText: "Upload photo",
                    imageBytes: _imageBytes,
                    setImageBytes: _setImageBytes,
                  ),
                  InputField(
                    leadingTitle: "Username",
                    controller: _usernameController,
                    validator: (value) => value.isEmpty ? "Username" : null,
                  ),
                  InputField(
                    leadingTitle: "Email",
                    controller: _emailController,
                    validator: (value) => value.isEmpty ? "Email" : null,
                  ),
                  InputField(
                    leadingTitle: "Headline",
                    controller: _headlineController,
                    validator: (value) => value.isEmpty ? "Headline" : null,
                  ),
                  InputField(
                    leadingTitle: "Description",
                    controller: _descriptionController,
                    validator: (value) => value.isEmpty ? "Description" : null,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Wrap(
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      Text(
                        "Change Password",
                        style: Theme.of(context).textTheme.bodyText2,
                      ),
                      SizedBox(
                        width: size.width * 0.2,
                      ),
                      DropdownButton(
                        value: _value,
                        items: [
                          DropdownMenuItem(
                            child: Text("Reset via Mail"),
                            value: 1,
                          ),
                          DropdownMenuItem(
                            child: Text("Reset via Text Message"),
                            value: 2,
                          ),
                        ],
                        onChanged: (value) {
                          setState(() {
                            _value = value;
                          });
                        },
                      ),
                    ],
                  ),
                  RoundedButton(
                    text: "Save Changes",
                    press: submitForm,
                    widthRatio: 0.8,
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            width: size.width * 0.02,
          ),
        ],
      ),
    );
  }
}
