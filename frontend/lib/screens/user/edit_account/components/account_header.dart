import 'package:flutter/material.dart';

class AccountHeader extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            Theme.of(context).primaryColorLight,
            Theme.of(context).primaryColor
          ],
        ),
      ),
      child: Padding(
        padding: EdgeInsets.only(top: 15, bottom: 15),
        child: Center(
          child: Column(
            children: [
              CircleAvatar(
                backgroundImage: NetworkImage(
                  "https://www.trendrr.net/wp-content/uploads/2017/06/Deepika-Padukone-1.jpg",
                ),
                radius: 50.0,
              ),
              SizedBox(height: size.height * 0.02),
              Text(
                "Lin",
                style: TextStyle(
                  fontSize: 25.0,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: size.height * 0.01),
              Text(
                "Bucharest, 2nd Sector",
                style: TextStyle(
                  fontSize: 20.0,
                  color: Colors.white,
                ),
              ),
              SizedBox(height: size.height * 0.02),
              Card(
                margin: EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
                clipBehavior: Clip.antiAlias,
                color: Colors.white,
                elevation: 5.0,
                child: Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: 10.0, vertical: 22.0),
                  child: Expanded(
                    child: Text(
                      "What the pug is wrong with you?",
                      style: TextStyle(
                        color: Theme.of(context).primaryColor,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
