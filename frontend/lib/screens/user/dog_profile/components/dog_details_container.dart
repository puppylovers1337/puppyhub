import 'dart:ui';
import 'package:flutter/material.dart';

class DogDetails extends StatelessWidget {
  final String name, breed, gender, description;
  static const IconData pets = IconData(0xe90e, fontFamily: 'MaterialIcons');

  DogDetails({
    Key key,
    this.name,
    this.breed,
    this.gender,
    this.description,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(top: 10, left: 40),
          child: Text(
            name,
            style: TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        SizedBox(height: size.height * 0.01),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              breed,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 10),
              child: CircleAvatar(
                radius: 16,
                child: CircleAvatar(
                  child: Icon(
                    pets,
                    color: gender == "Male" ? Colors.blue : Colors.pink,
                  ),
                  backgroundColor: Colors.white,
                  radius: 15,
                ),
              ),
            ),
            Text(
              gender,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ),
        SizedBox(height: size.height * 0.001),
        Divider(thickness: 3, color: Theme.of(context).primaryColor),
        Padding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 40),
          child: Text(description),
        ),
      ],
    ));
  }
}
