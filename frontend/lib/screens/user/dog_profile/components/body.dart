import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';

import 'dog_details_container.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Container(
            child: Image(
              image: AssetImage('assets/images/1915.jpg'),
            ),
            height: size.height / 4,
            width: size.width,
            decoration: BoxDecoration(
              border:
                  Border.all(color: Theme.of(context).primaryColor, width: 3),
            ),
          ),
          Container(
            child: Column(children: <Widget>[
              DogDetails(
                name: "Max",
                gender: "Male",
                description:
                    "Dogs have four legs and make a bark sound.\n Dogs often chase cats, and most dogs will fetch a ball or stick.\n Dogs can smell and hear better than humans, but cannot see well in color because they are color blind. \nDue to the anatomy of the eye, dogs can see better in dim light than humans.",
                breed: "Greyhound",
              ),
              RoundedButton(
                text: "Edit Profile",
                press: () {
                  Navigator.of(context).pushNamed('/dog/edit');
                },
                widthRatio: 0.8,
              ),
              RoundedButton(
                text: "Delete Profile",
                press: () {},
                widthRatio: 0.8,
              ),
            ]),
          )
        ],
      ),
    );
  }
}
