import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/dog.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/util/string_extension.dart';

import 'components/dog_details_container.dart';

class DogProfile extends StatelessWidget {
  final String dogId;

  const DogProfile({
    Key key,
    @required this.dogId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    Dog dog = context.select<UserState, Dog>((state) => state.getDog(dogId));

    if (dog == null) {
      return Scaffold();
    }

    return Scaffold(
      appBar: BaseAppBar(
        title: 'Dog\'s Profile',
        hasArrowBack: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              child: Image(
                image: context.select<UserState, ImageProvider>(
                    (state) => state.getDogImage(dogId)),
              ),
              height: size.height / 4,
              width: size.width,
              decoration: BoxDecoration(
                border:
                    Border.all(color: Theme.of(context).primaryColor, width: 3),
              ),
            ),
            Container(
              child: Column(children: <Widget>[
                DogDetails(
                  name: dog.name,
                  gender: EnumToString.convertToString(dog.gender).capitalize(),
                  description: dog.description,
                  breed: dog.breed,
                ),
                RoundedButton(
                  text: "Edit Profile",
                  press: () {
                    Navigator.of(context)
                        .pushNamed('/dog/edit', arguments: {'dogId': dogId});
                  },
                  widthRatio: 0.8,
                ),
                RoundedButton(
                  text: "Delete Profile",
                  press: () async {
                    // TODO: popup message if successfully deleted
                    await context.read<UserState>().deleteDog(dogId);
                    Navigator.of(context).pop();
                  },
                  widthRatio: 0.8,
                ),
              ]),
            )
          ],
        ),
      ),
    );
  }
}
