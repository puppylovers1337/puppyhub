import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';

import 'card_container.dart';

class DogCard extends StatelessWidget {
  final String dogId;

  const DogCard({
    Key key,
    @required this.dogId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return CardContainer(
      child: Card(
        child: Container(
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(8.0),
                  child: Container(
                    height: double.infinity,
                    width: double.infinity,
                    child: FittedBox(
                      child: Image(
                        image: context.select<UserState, ImageProvider>(
                            (state) => state.getDogImage(dogId)),
                      ),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              SizedBox(
                width: size.width * 0.05,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      context.select<UserState, String>(
                          (state) => state.getDog(dogId).name),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    RoundedButton(
                      text: "Match",
                      press: () {
                        Navigator.of(context).pushNamed('/match/filters',
                            arguments: {'dogId': dogId});
                      },
                      margin: EdgeInsets.symmetric(vertical: 5),
                      backgroundColor: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                    ),
                    RoundedButton(
                      text: "Profile",
                      press: () {
                        Navigator.of(context)
                            .pushNamed("/dog", arguments: {'dogId': dogId});
                      },
                      backgroundColor: Colors.white,
                      textColor: Theme.of(context).primaryColor,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
