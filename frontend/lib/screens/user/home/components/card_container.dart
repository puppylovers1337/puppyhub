import 'package:flutter/material.dart';

class CardContainer extends StatelessWidget {
  final Card child;

  const CardContainer({
    Key key,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 5),
      height: 220,
      width: double.infinity,
      child: child,
    );
  }
}
