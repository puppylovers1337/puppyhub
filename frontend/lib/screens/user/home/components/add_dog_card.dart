import 'package:flutter/material.dart';

import 'card_container.dart';

class AddDogCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return CardContainer(
      child: Card(
        child: IconButton(
          icon: const Icon(Icons.add, size: 60.0),
          tooltip: 'Add dog',
          onPressed: () {
            Navigator.of(context).pushNamed("/dog/new");
          },
        ),
      ),
    );
  }
}
