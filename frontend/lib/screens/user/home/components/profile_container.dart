import 'package:flutter/material.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:provider/provider.dart';

class ProfileContainer extends StatelessWidget {
  final String username;

  const ProfileContainer({
    Key key,
    @required this.username,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      width: double.infinity,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: 30.0),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed('/account');
              },
              child: CircleAvatar(
                radius: 44,
                backgroundColor: Theme.of(context).primaryColorLight,
                child: CircleAvatar(
                  radius: 40,
                  backgroundImage: context.select<UserState, ImageProvider>(
                      (state) => state.getProfileImage()),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 30.0),
            child: Text(
              this.username,
              style: TextStyle(
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold,
                fontSize: 15,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
