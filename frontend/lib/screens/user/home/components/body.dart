import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/models/dog.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'add_dog_card.dart';
import 'dog_card.dart';
import 'profile_container.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      height: size.height,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Selector<UserState, String>(
            selector: (context, provider) => provider.user?.username,
            builder: (context, username, child) {
              if (username == null) {
                return CircularProgressIndicator();
              } else {
                return ProfileContainer(username: username);
              }
            },
          ),
          Flexible(
            child: Selector<UserState, List<Dog>>(
              selector: (context, data) => data.getDogs().toList(),
              builder: (context, dogs, child) {
                return ListView.builder(
                  padding: const EdgeInsets.all(8),
                  itemCount: dogs.length + 1,
                  itemBuilder: (BuildContext context, int index) =>
                      index < dogs.length
                          ? DogCard(dogId: dogs[index].id)
                          : AddDogCard(),
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
