import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';

import 'background.dart';

class Body extends StatelessWidget {
  final TextStyle style = TextStyle(fontFamily: 'Roboto', fontSize: 20.0);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "WELCOME TO PUPPY HUB",
            style: Theme.of(context).textTheme.headline6,
          ),
          Image.asset(
            'assets/images/logo.jpg',
            height: size.height * 0.45,
          ),
          RoundedButton(
            text: "LOGIN",
            widthRatio: 0.7,
            press: () {
              Navigator.of(context).pushNamed('/login');
            },
          ),
          RoundedButton(
            text: "SIGN UP",
            widthRatio: 0.7,
            press: () {
              Navigator.of(context).pushNamed('/register');
            },
            textColor: Colors.black,
            backgroundColor: Theme.of(context).primaryColorLight,
          )
        ],
      ),
    );
  }
}
