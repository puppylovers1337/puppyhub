import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/market_post.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:puppy_hub/util/converter.dart';

import 'market_post_card.dart';

class Body extends StatefulWidget {
  @override
  _BodyState createState() => _BodyState();
}

enum PostsType { my_posts, all_posts }

class _BodyState extends State<Body> {
  bool _showButtonHeader = true;
  ScrollController _scrollController = new ScrollController();
  bool isScrollingDown = false;
  List<MarketPostCard> postCards = [];
  PostsType postsType = PostsType.all_posts;

  void loadPosts(PostsType neededPostsType) async {
    postCards = [];
    var response = neededPostsType == PostsType.all_posts
        ? await Api.get('marketposts')
        : await Api.get('marketposts/me');
    var postsJson = List<dynamic>.from(response);
    postsJson.forEach((postJson) {
      MarketPost post = MarketPost.fromJson(postJson);
      MarketPostCard newPostCard = MarketPostCard(
        postTitle: post.title,
        location: post.location,
        description: post.description,
        postType: post.type,
        image: post.images.isEmpty ? null : post.images.last,
        date: Converter.toMarketPostFormat(post.createdAt),
        onTap: () => onPostTap(post.id),
      );
      postCards.add(newPostCard);
    });

    postCards.sort((a, b) => -a.date.compareTo(b.date));

    setState(() {
      postsType = neededPostsType;
      postCards = postCards;
    });
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.reverse) {
        if (!isScrollingDown) {
          setState(() {
            isScrollingDown = true;
            _showButtonHeader = false;
          });
        }
      }
      if (_scrollController.position.userScrollDirection ==
          ScrollDirection.forward) {
        if (isScrollingDown) {
          setState(() {
            isScrollingDown = false;
            _showButtonHeader = true;
          });
        }
      }
    });
    loadPosts(postsType);
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void onPostTap(String postId) {
    Navigator.of(context).pushNamed('/market/post',
        arguments: {'postId': postId}).then((_) => loadPosts(postsType));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AnimatedContainer(
          duration: Duration(milliseconds: 300),
          height: _showButtonHeader ? 70 : 0.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              RoundedButton(
                text: "New post",
                widthRatio: 0.40,
                press: () {
                  Navigator.of(context)
                      .pushNamed('/market/post/new')
                      .then((_) => loadPosts(postsType));
                },
              ),
              postsType == PostsType.all_posts
                  ? RoundedButton(
                      text: "My posts",
                      widthRatio: 0.40,
                      press: () {
                        loadPosts(PostsType.my_posts);
                      },
                    )
                  : RoundedButton(
                      text: "All posts",
                      widthRatio: 0.40,
                      press: () {
                        loadPosts(PostsType.all_posts);
                      },
                    ),
            ],
          ),
        ),
        Expanded(
          child: postCards.isEmpty
              ? Align(
                  alignment: Alignment.center,
                  child: Text("No post available."),
                )
              : SingleChildScrollView(
                  controller: _scrollController,
                  child: Column(
                    children: postCards,
                  ),
                ),
        ),
      ],
    );
  }
}
