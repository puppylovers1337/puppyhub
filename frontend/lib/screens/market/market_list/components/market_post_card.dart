import 'package:flutter/material.dart';
import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/models/market_post.dart';
import 'package:puppy_hub/screens/market/components/badge.dart';

class MarketPostCard extends StatelessWidget {
  final String location, description, date, image, postTitle;
  final Function onTap;
  final PostType postType;

  const MarketPostCard({
    Key key,
    this.location,
    this.description,
    this.postType,
    this.date,
    this.image,
    this.postTitle,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return GestureDetector(
      onTap: this.onTap,
      child: Padding(
        padding: EdgeInsets.all(15),
        child: Container(
          decoration: new BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 4.0,
                spreadRadius: 0.0,
              )
            ],
            borderRadius: BorderRadius.all(Radius.circular(10)),
          ),
          child: Stack(
            children: [
              Positioned(
                top: 5,
                left: 5,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: postType == PostType.sell ? sellBadge : babysitBadge,
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: 15,
                  vertical: 10,
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          flex: 1,
                          child: Column(
                            children: [
                              Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  postTitle,
                                  style: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              SizedBox(height: size.height * 0.013),
                              Row(
                                children: [
                                  Icon(
                                    Icons.location_pin,
                                    size: 14,
                                    color: Theme.of(context).primaryColor,
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10),
                                    child: Text(
                                      location,
                                      style: TextStyle(
                                        color: Colors.grey,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20.0),
                            child: image != null
                                ? Image.network(kApiBaseUrl + 'images/$image')
                                : Image.network(kDefaultImageUrl),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: size.height * 0.02),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Text(description),
                    ),
                    SizedBox(height: size.height * 0.015),
                    Align(
                      alignment: Alignment.bottomRight,
                      child: Text(
                        date,
                        style: TextStyle(
                          fontSize: 11,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
