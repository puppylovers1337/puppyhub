import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';
import 'package:puppy_hub/screens/chat/chat_list/components/avatar.dart';

import 'components/body.dart';

class MarketListScreen extends StatelessWidget {
  const MarketListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Market',
        leadingWidget: Avatar(
          onPressed: () {},
        ),
      ),
      body: Body(),
    );
  }
}
