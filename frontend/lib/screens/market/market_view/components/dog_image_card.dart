import 'package:flutter/material.dart';
import 'package:puppy_hub/common/constants.dart';

class DogImageCard extends StatelessWidget {
  final String image;

  DogImageCard({Key key, this.image}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: image != null
          ? Image.network(kApiBaseUrl + 'images/$image')
          : Image.network(kDefaultImageUrl),
    );
  }
}
