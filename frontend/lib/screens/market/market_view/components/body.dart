import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/market_post.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/screens/market/components/badge.dart';
import 'package:puppy_hub/util/validator.dart';

import 'contact_info_card.dart';
import 'dog_image_card.dart';

class Body extends StatefulWidget {
  final String postId;

  const Body({Key key, this.postId}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  MarketPost marketPost = MarketPost();
  String postImage;
  String price;

  void fetchPost() async {
    var marketPostJson = await Api.get('marketposts/${widget.postId}');
    marketPost = MarketPost.fromJson(marketPostJson);
    if (marketPost.images.isNotEmpty) {
      postImage = marketPost.images.last;
    }

    price = Validator.numericCheck.hasMatch(marketPost.price)
        ? marketPost.price + " \$"
        : marketPost.price;

    setState(() {
      marketPost = marketPost;
      postImage = postImage;
    });
  }

  @override
  void initState() {
    super.initState();
    fetchPost();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Expanded(
          child: SingleChildScrollView(
            child: Column(
              children: [
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        marketPost.type == PostType.sell
                            ? sellBadge
                            : babysitBadge,
                        Text(
                          marketPost.title != null ? marketPost.title : "",
                          style: TextStyle(
                            fontSize: 21,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          width: 35,
                        ),
                      ]),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20),
                  child: DogImageCard(image: postImage),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20, left: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Icon(Icons.location_pin),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Text(
                              marketPost.location != null
                                  ? marketPost.location
                                  : "",
                              style: TextStyle(
                                fontSize: 16,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 30),
                        child: Text(
                          marketPost.price != null ? price.toUpperCase() : "",
                          style: TextStyle(
                            fontSize: 19,
                            fontWeight: FontWeight.bold,
                            color: Colors.green,
                          ),
                          textAlign: TextAlign.right,
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: 15, right: 15, top: 15, bottom: 10),
                  child: Container(
                    decoration: new BoxDecoration(
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 2.0,
                          spreadRadius: 0.0,
                        )
                      ],
                      borderRadius: BorderRadius.all(Radius.circular(10)),
                    ),
                    child: Align(
                      alignment: Alignment.topLeft,
                      child: Padding(
                        padding: EdgeInsets.all(15),
                        child: Text(
                          marketPost.description != null
                              ? marketPost.description
                              : "",
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                ContactInfoCard(
                  phoneNumber: marketPost.phoneNumber,
                  email: marketPost.email,
                ),
              ],
            ),
          ),
        ),
        if (marketPost.userId == context.read<UserState>().user.id)
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.only(bottom: 10, top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  RoundedButton(
                    text: "Edit post",
                    press: () {
                      Navigator.of(context).pushNamed('/market/post/edit',
                          arguments: {
                            'postId': widget.postId
                          }).then((_) => fetchPost());
                    },
                    widthRatio: 0.45,
                  ),
                  RoundedButton(
                    text: "Delete post",
                    press: () async {
                      await Api.delete('marketposts/${widget.postId}');
                      Navigator.of(context).pop();
                    },
                    widthRatio: 0.45,
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }
}
