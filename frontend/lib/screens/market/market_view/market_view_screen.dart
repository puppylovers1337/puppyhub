import 'package:flutter/material.dart';
import 'components/body.dart';

class MarketViewScreen extends StatelessWidget {
  final String postId;

  const MarketViewScreen({Key key, this.postId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Market post"),
      ),
      body: Body(postId: postId),
    );
  }
}
