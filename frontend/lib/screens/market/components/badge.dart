import 'package:flutter/material.dart';

class Badge extends StatelessWidget {
  final IconData iconData;
  final double size;

  const Badge({Key key, this.iconData, this.size}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: Material(
        color: Theme.of(context).primaryColorLight,
        child: InkWell(
          child: SizedBox(
            width: this.size,
            height: this.size,
            child: Icon(
              iconData,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}

Badge sellBadge = Badge(
  iconData: Icons.attach_money,
  size: 35,
);
Badge babysitBadge = Badge(
  iconData: Icons.pets,
  size: 35,
);
