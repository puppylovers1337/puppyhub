import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/util/image.dart' as ImageHelper;

class ChangePhotoContainer extends StatefulWidget {
  final Function(Uint8List) setImageBytes;
  final Uint8List fileBytes;

  ChangePhotoContainer({
    Key key,
    this.setImageBytes,
    this.fileBytes,
  }) : super(key: key);

  @override
  _ChangePhotoContainerState createState() => _ChangePhotoContainerState();
}

class _ChangePhotoContainerState extends State<ChangePhotoContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Image.memory(
                  widget.fileBytes,
                  width: 100,
                  height: 100,
                  fit: BoxFit.fitWidth,
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.only(right: 10),
                child: RoundedButton(
                  text: "Change photo",
                  press: () async {
                    var imageBytes = await ImageHelper.readImageAsBytes();
                    if (imageBytes == null) return;
                    widget.setImageBytes(imageBytes);
                  },
                ),
              ),
            ),
          ],
        ));
  }
}
