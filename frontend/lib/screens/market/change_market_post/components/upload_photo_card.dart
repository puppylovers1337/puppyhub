import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:puppy_hub/util/image.dart' as ImageHelper;

import 'change_photo_container.dart';

class UploadImageSection extends StatefulWidget {
  final Function(Uint8List) setImageBytes;
  final Uint8List fileBytes;

  UploadImageSection({
    Key key,
    this.setImageBytes,
    this.fileBytes,
  }) : super(key: key);

  @override
  _UploadImageSectionState createState() => _UploadImageSectionState();
}

class _UploadImageSectionState extends State<UploadImageSection> {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: widget.fileBytes != null
          ? ChangePhotoContainer(
              setImageBytes: widget.setImageBytes,
              fileBytes: widget.fileBytes,
            )
          : GestureDetector(
              onTap: () async {
                var imageBytes = await ImageHelper.readImageAsBytes();
                if (imageBytes == null) return;
                widget.setImageBytes(imageBytes);
              },
              child: Container(
                width: size.width * 0.8,
                decoration: new BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey,
                      blurRadius: 4.0,
                      spreadRadius: 0.0,
                    )
                  ],
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                ),
                child: Column(
                  children: [
                    Icon(
                      Icons.add,
                    ),
                    Padding(
                      padding: EdgeInsets.only(bottom: 10),
                      child: Text(
                        "Upload optional photo",
                        style: TextStyle(color: Colors.grey),
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
