import 'dart:typed_data';

import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/material.dart';
import 'package:network_image_to_byte/network_image_to_byte.dart';
import 'package:puppy_hub/common/constants.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/market_post.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:puppy_hub/screens/market/change_market_post/components/numeric_leading_input_field.dart';
import 'package:puppy_hub/util/image.dart' as ImageHelper;
import 'package:puppy_hub/util/validator.dart';

import 'description_input_field.dart';
import 'leading_input_field.dart';
import 'upload_photo_card.dart';

class MarketPostForm extends StatefulWidget {
  final String postId;

  const MarketPostForm({Key key, this.postId}) : super(key: key);

  @override
  _MarketPostFormState createState() => _MarketPostFormState();
}

enum FormType { create, edit }

class _MarketPostFormState extends State<MarketPostForm> {
  final _titleController = TextEditingController();
  final _locationController = TextEditingController();
  final _priceController = TextEditingController();
  final _descriptionController = TextEditingController();
  final _phoneNumberController = TextEditingController();
  final _emailController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  String errorMessage;

  FormType formType;
  MarketPost post = MarketPost();
  Uint8List _newImageBytes;
  Uint8List _imageBytes;
  List<bool> _isSelected = [true, false];

  void fetchPost() async {
    var postJson = await Api.get('marketposts/${widget.postId}');
    post = MarketPost.fromJson(postJson);
    _titleController.text = post?.title;
    _locationController.text = post?.location;
    _descriptionController.text = post?.description;
    if (Validator.numericCheck.hasMatch(post?.price)) {
      _priceController.text = post?.price;
    }
    _phoneNumberController.text = post?.phoneNumber;
    _emailController.text = post?.email;

    if (post.images.isNotEmpty) {
      _imageBytes =
          await networkImageToByte(kApiBaseUrl + 'images/${post.images.last}');
    }

    _isSelected = post.type == PostType.sell ? [true, false] : [false, true];

    setState(() {
      _imageBytes = _imageBytes;
      _isSelected = _isSelected;
      post = post;
    });
  }

  @override
  void initState() {
    super.initState();
    formType = widget.postId == null ? FormType.create : FormType.edit;
    post.type = PostType.sell;

    if (formType == FormType.edit) {
      fetchPost();
    }
  }

  @override
  void dispose() {
    super.dispose();
    _titleController.dispose();
    _locationController.dispose();
    _priceController.dispose();
    _descriptionController.dispose();
    _phoneNumberController.dispose();
    _emailController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Function submitForm = () async {
      if (formKey.currentState.validate()) {
        dynamic data = {
          "title": _titleController.text,
          "description": _descriptionController.text,
          if (_priceController.text.isNotEmpty) "price": _priceController.text,
          if (_priceController.text.isEmpty) "price": "Negociable",
          "type": EnumToString.convertToString(post.type).toUpperCase(),
          "location": _locationController.text,
          "email": _emailController.text,
          "phoneNumber": _phoneNumberController.text,
        };

        if (formType == FormType.create) {
          var postJson = await Api.post('marketposts', data);
          post = MarketPost.fromJson(postJson);
        } else {
          await Api.put('marketposts/${widget.postId}', data);
        }

        if (_newImageBytes != null) {
          var formData = ImageHelper.getFormData(_newImageBytes);
          await Api.put("marketposts/${post.id}/images", formData);
        }

        Navigator.of(context).pop();
      }
    };

    Function _setImageBytes = (Uint8List imageBytes) {
      setState(() {
        _newImageBytes = imageBytes;
      });
    };

    return Form(
      key: formKey,
      child: SingleChildScrollView(
        child: Column(
          children: [
            if (errorMessage != null)
              Text(
                errorMessage,
                style: TextStyle(
                  color: Colors.red,
                ),
              ),
            Padding(
              padding: EdgeInsets.only(top: 20, bottom: 10),
              child: Text(
                "Post type",
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            ToggleButtons(
              selectedColor: Colors.white,
              borderRadius: BorderRadius.circular(5),
              fillColor: Theme.of(context).primaryColor,
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 1.5),
                  ),
                  alignment: Alignment.center,
                  width: size.width * 0.4,
                  child: Text(
                    "SELL",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: Colors.black, width: 1.5),
                  ),
                  alignment: Alignment.center,
                  width: size.width * 0.4,
                  child: Text(
                    "BABYSIT",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
              isSelected: _isSelected,
              onPressed: (int index) {
                setState(() {
                  for (int buttonIndex = 0;
                      buttonIndex < _isSelected.length;
                      buttonIndex++) {
                    if (buttonIndex == index) {
                      _isSelected[buttonIndex] = true;
                      post.type = index == 0 ? PostType.sell : PostType.babysit;
                    } else {
                      _isSelected[buttonIndex] = false;
                    }
                  }
                });
              },
            ),
            SizedBox(height: 10),
            LeadingInputField(
              leadingTitle: "Post title",
              controller: _titleController,
              validator: (value) => Validator.mandatory("Title", value),
            ),
            LeadingInputField(
              leadingTitle: "Location",
              controller: _locationController,
              validator: (value) => Validator.mandatory("Location", value),
            ),
            NumericLeadingInputField(
              leadingTitle: "Price \$\$\$",
              controller: _priceController,
              validator: Validator.optionalPrice,
              hintText: "Negociable",
            ),
            DescriptionInputField(
              hintText: "Type...",
              controller: _descriptionController,
              validator: (value) => Validator.mandatory("Description", value),
            ),
            SizedBox(height: size.height * 0.04),
            //TODO : add price and type of post
            Align(
              alignment: Alignment.centerLeft,
              child: Padding(
                padding: EdgeInsets.only(left: 30),
                child: Text(
                  "CONTACT INFO:",
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ),
            Divider(
              color: Theme.of(context).primaryColor,
              height: 20,
              thickness: 5,
              indent: 30,
              endIndent: 30,
            ),
            NumericLeadingInputField(
              leadingTitle: "Phone number",
              controller: _phoneNumberController,
              validator: (value) => Validator.mandatory("Phone number", value),
            ),
            LeadingInputField(
              leadingTitle: "Email",
              controller: _emailController,
              validator: Validator.email,
            ),
            SizedBox(height: size.height * 0.06),
            UploadImageSection(
              setImageBytes: _setImageBytes,
              fileBytes: _newImageBytes != null ? _newImageBytes : _imageBytes,
            ),
            SizedBox(height: size.height * 0.05),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: EdgeInsets.only(bottom: 10, top: 10),
                child: RoundedButton(
                  text:
                      formType == FormType.create ? "Add post" : "Save Changes",
                  press: submitForm,
                  widthRatio: 0.9,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
