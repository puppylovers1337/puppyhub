import 'package:flutter/material.dart';

class LeadingInputField extends StatelessWidget {
  final String leadingTitle;
  final TextEditingController controller;
  final Function(String) validator;

  const LeadingInputField({
    Key key,
    this.leadingTitle,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              leadingTitle,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: TextFormField(
              controller: this.controller,
              validator: this.validator,
            ),
          ),
        ],
      ),
    );
  }
}
