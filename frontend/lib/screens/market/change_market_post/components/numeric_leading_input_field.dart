import 'package:flutter/material.dart';

class NumericLeadingInputField extends StatelessWidget {
  final String leadingTitle;
  final String hintText;
  final TextEditingController controller;
  final Function(String) validator;

  const NumericLeadingInputField({
    Key key,
    this.leadingTitle,
    this.hintText,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            flex: 1,
            child: Text(
              leadingTitle,
              style: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: TextFormField(
              decoration: InputDecoration(
                hintText: this.hintText,
                hintStyle: TextStyle(color: Colors.grey),
              ),
              keyboardType: TextInputType.number,
              controller: this.controller,
              validator: this.validator,
            ),
          ),
        ],
      ),
    );
  }
}
