import "package:flutter/material.dart";
import 'package:puppy_hub/screens/market/change_market_post/components/market_post_form.dart';

class ChangeMarketPostScreen extends StatelessWidget {
  final String postId;

  ChangeMarketPostScreen({
    Key key,
    this.postId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String _title = (postId == null ? "Create" : "Edit") + " post";

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(_title),
      ),
      body: MarketPostForm(postId: postId),
    );
  }
}
