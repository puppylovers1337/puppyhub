import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';
import 'package:puppy_hub/models/match_filters.dart';

import 'components/body.dart';

class MatchScreen extends StatelessWidget {
  final String dogId;
  final MatchFilters filters;

  const MatchScreen({
    Key key,
    this.dogId,
    this.filters,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: BaseAppBar(
        title: 'Match',
        hasArrowBack: true,
      ),
      body: Body(dogId: dogId, filters: filters),
    );
  }
}
