import 'package:enum_to_string/enum_to_string.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:puppy_hub/models/dog.dart';
import 'package:puppy_hub/models/match_filters.dart';
import 'package:puppy_hub/models/state/user_state.dart';
import 'package:puppy_hub/networking/api/api.dart';
import 'package:provider/provider.dart';
import 'package:puppy_hub/util/random.dart';
import 'package:puppy_hub/util/string_extension.dart';

import 'match_card.dart';

class Body extends StatefulWidget {
  final String dogId;
  final MatchFilters filters;

  const Body({
    Key key,
    this.dogId,
    this.filters,
  }) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  List<Dog> _dogs;

  void _updateDogs() {
    setState(() {
      _dogs = _dogs;
    });
  }

  Future<void> _loadDogs(Dog userDog) async {
    var longitude = userDog.location.coordinates[0];
    var latitude = userDog.location.coordinates[1];
    var sex = EnumToString.convertToString(userDog.gender) == 'male'
        ? "female"
        : "male";
    var range = widget.filters.range;

    var query = 'match/?range=$range&longitude=$longitude&latitude=$latitude';

    if (widget.filters.breeds.isNotEmpty) {
      var breed = widget.filters.breeds[0];
      query += '&breed=$breed';
    }

    if (widget.filters.interestedIn == InterestType.breeding) {
      query += '&sex=$sex';
    }

    var dogs = await Api.get(query);

    _dogs = List<Dog>.from(
        dogs.map((dog) => Dog.fromJson(dog as Map<String, dynamic>)));

    _updateDogs();
  }

  void _onCancel() {
    _dogs.removeLast();
    _updateDogs();
  }

  Future<void> _onLike() async {
    var data = {"targetDogId": _dogs.last.id, "sourceDogId": widget.dogId};
    _dogs.removeLast();
    await Api.post('match/', data);
    _updateDogs();
  }

  String getRandomBadge(int lenBadges) {
    int rndInt = Rnd.generator.nextInt(lenBadges + 1);
    if (rndInt == 0) return '';
    return 'assets/images/prize_$rndInt.png';
  }

  Widget getMatchCard() {
    if (_dogs == null || _dogs.isEmpty) {
      return Center(
        child: Text("Walk around until new dogs arrive."),
      );
    } else {
      var dog = _dogs.last;
      return MatchCard(
        name: dog.name,
        gender: EnumToString.convertToString(dog.gender).capitalize(),
        description: dog.description,
        breed: dog.breed.capitalize(),
        onCancel: _onCancel,
        onLike: _onLike,
      );
    }
  }

  @override
  void initState() {
    super.initState();
    _loadDogs(context.read<UserState>().getDog(widget.dogId));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    Image getImage() {
      return _dogs == null || _dogs.isEmpty
          ? Image.asset('assets/images/logo.jpg', height: size.height * 0.35)
          : Image(
              image: context.read<UserState>().getMatchDogImage(_dogs.last));
    }

    print(getRandomBadge(8));
    return Column(
      children: [
        Expanded(
          flex: 2,
          child: getImage(),
        ),
        Expanded(
          flex: 3,
          child: Container(
            height: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.white, Theme.of(context).primaryColor],
              ),
            ),
            child: Stack(
              clipBehavior: Clip.none,
              children: [
                if (!(_dogs == null || _dogs.isEmpty))
                  Builder(builder: (context) {
                    String imagePath = getRandomBadge(8);
                    if (imagePath != '') {
                      return Positioned(
                        top: -45,
                        right: 10,
                        child: Image.asset(
                          imagePath,
                          height: 100,
                        ),
                      );
                    }
                    return SizedBox();
                  }),
                Padding(
                  padding: const EdgeInsets.only(top: 10),
                  child: getMatchCard(),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
