import 'package:flutter/material.dart';

class IconWithText extends StatelessWidget {
  const IconWithText({
    Key key,
    @required this.icon,
    @required this.text,
  }) : super(key: key);

  final Icon icon;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 16,
            child: CircleAvatar(
              child: icon,
              backgroundColor: Colors.white,
              radius: 15,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
