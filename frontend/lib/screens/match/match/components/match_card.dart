import 'package:flutter/material.dart';

class MatchCard extends StatelessWidget {
  final String name, breed, gender, description;
  static const IconData pets = IconData(0xe90e, fontFamily: 'MaterialIcons');
  static const IconData group_outlined =
      IconData(57873, fontFamily: 'MaterialIcons');
  final Function onCancel, onLike;

  MatchCard({
    Key key,
    this.name,
    this.breed,
    this.gender,
    this.description,
    @required this.onCancel,
    @required this.onLike,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left: size.width * 0.2),
            child: Text(
              name,
              style: TextStyle(
                fontSize: 30,
                fontFamily: 'Lobster',
                fontWeight: FontWeight.bold,
                color: Colors.blueGrey[600],
              ),
            ),
          ),
          SizedBox(height: size.height * 0.02),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconWithText(
                icon: Icon(group_outlined),
                text: breed,
              ),
              IconWithText(
                icon: Icon(
                  pets,
                  color: gender == "Male" ? Colors.blue : Colors.pink,
                ),
                text: gender,
              ),
            ],
          ),
          SizedBox(height: size.height * 0.001),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Text(
                  description,
                  style: TextStyle(
                    fontSize: 14.0,
                    fontStyle: FontStyle.italic,
                  ),
                ),
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              RawMaterialButton(
                onPressed: this.onCancel,
                elevation: 2.0,
                fillColor: Colors.white,
                child: Icon(
                  Icons.close,
                  color: Colors.grey,
                ),
                padding: EdgeInsets.all(15.0),
                shape: CircleBorder(),
              ),
              SizedBox(
                width: size.width * 0.05,
              ),
              RawMaterialButton(
                onPressed: this.onLike,
                elevation: 2.0,
                fillColor: Colors.white,
                child: Icon(
                  Icons.favorite,
                  color: Colors.pink[400],
                ),
                padding: EdgeInsets.all(15.0),
                shape: CircleBorder(),
              ),
            ],
          ),
          SizedBox(
            height: size.height * 0.02,
          )
        ],
      ),
    );
  }
}

class IconWithText extends StatelessWidget {
  const IconWithText({
    Key key,
    @required this.icon,
    @required this.text,
  }) : super(key: key);

  final Icon icon;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: <Widget>[
          CircleAvatar(
            radius: 16,
            child: CircleAvatar(
              child: icon,
              backgroundColor: Colors.white,
              radius: 15,
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10),
            child: Text(
              text,
              style: TextStyle(
                fontWeight: FontWeight.bold,
              ),
            ),
          )
        ],
      ),
    );
  }
}
