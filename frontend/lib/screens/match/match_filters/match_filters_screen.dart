import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';

import 'components/body.dart';

class MatchFiltersScreen extends StatelessWidget {
  final String dogId;
  const MatchFiltersScreen({Key key, this.dogId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: BaseAppBar(
        title: 'Match Filters',
        hasArrowBack: true,
      ),
      body: Body(dogId: this.dogId),
    );
  }
}
