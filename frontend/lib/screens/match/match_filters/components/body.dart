import 'package:flutter/material.dart';
import 'package:multi_select_flutter/chip_display/multi_select_chip_display.dart';
import 'package:multi_select_flutter/dialog/multi_select_dialog_field.dart';
import 'package:multi_select_flutter/util/multi_select_item.dart';
import 'package:multi_select_flutter/util/multi_select_list_type.dart';
import 'package:puppy_hub/components/rounded_button.dart';
import 'package:puppy_hub/models/match_filters.dart';
import 'package:puppy_hub/networking/api/api.dart';

class Body extends StatefulWidget {
  final String dogId;

  const Body({Key key, this.dogId}) : super(key: key);

  @override
  _BodyState createState() => _BodyState();
}

class _BodyState extends State<Body> {
  MatchFilters _filters = MatchFilters();
  List<String> availableBreeds = [];
  final List<bool> _isSelected = [true, false];

  void _loadBreeds() async {
    var breeds = List<String>.from(await Api.get('dogs/breeds'));
    setState(() {
      availableBreeds = breeds;
    });
  }

  @override
  void initState() {
    super.initState();
    _loadBreeds();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Stack(children: [
      SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(top: 25, left: 20, right: 20),
          child: Column(
            children: [
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "INTERESTED IN:",
                  style: TextStyle(
                    color: Colors.grey.withOpacity(0.9),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Divider(
                height: 10,
                thickness: 2,
              ),
              ToggleButtons(
                selectedColor: Colors.white,
                borderRadius: BorderRadius.circular(5),
                fillColor: Theme.of(context).primaryColor,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 1.5),
                    ),
                    alignment: Alignment.center,
                    width: size.width * 0.4,
                    child: Text(
                      "BREEDING",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black, width: 1.5),
                    ),
                    alignment: Alignment.center,
                    width: size.width * 0.4,
                    child: Text(
                      "WALKING",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
                onPressed: (int index) {
                  setState(() {
                    for (int buttonIndex = 0;
                        buttonIndex < _isSelected.length;
                        buttonIndex++) {
                      if (buttonIndex == index) {
                        _isSelected[buttonIndex] = true;
                        _filters.interestedIn = index == 0
                            ? InterestType.breeding
                            : InterestType.walking;
                      } else {
                        _isSelected[buttonIndex] = false;
                      }
                    }
                  });
                },
                isSelected: _isSelected,
              ),
              SizedBox(height: size.height * 0.04),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "FILTER YOUR SEARCH:",
                  style: TextStyle(
                    color: Colors.grey.withOpacity(0.9),
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Divider(
                height: 10,
                thickness: 2,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: Text(
                  "BREED",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              MultiSelectDialogField(
                searchable: true,
                title: Text("Dog Breed"),
                items: this
                    .availableBreeds
                    .map((breed) => MultiSelectItem(breed, breed))
                    .toList(),
                listType: MultiSelectListType.LIST,
                onConfirm: (values) {
                  _filters.breeds = values;
                },
                chipDisplay: MultiSelectChipDisplay(
                  items: _filters.breeds
                      .map((e) => MultiSelectItem(e, e))
                      .toList(),
                  onTap: (value) {
                    setState(() {
                      _filters.breeds.remove(value);
                    });
                  },
                ),
              ),
              SizedBox(height: size.height * 0.02),
              Divider(
                height: 10,
                thickness: 4,
              ),
              Padding(
                padding: EdgeInsets.only(top: 20, bottom: 15),
                child: Text(
                  "MAXIMUM DISTANCE",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 15,
                  ),
                ),
              ),
              Slider(
                  value: _filters.range.toDouble(),
                  min: 0.0,
                  max: 200.0,
                  divisions: 20,
                  activeColor: Theme.of(context).primaryColor,
                  inactiveColor: Colors.grey.withOpacity(0.6),
                  label: _filters.range.toString() + " km",
                  onChanged: (double newValue) {
                    setState(() {
                      _filters.range = newValue.round();
                    });
                  }),
              SizedBox(
                height: size.height * 0.1,
              )
            ],
          ),
        ),
      ),
      Align(
          alignment: Alignment.bottomCenter,
          child: RoundedButton(
            text: "SAVE FILTERS",
            widthRatio: 0.8,
            press: () {
              Navigator.of(context).pushNamed('/match',
                  arguments: {'dogId': widget.dogId, 'filters': _filters});
            },
          )),
    ]);
  }
}
