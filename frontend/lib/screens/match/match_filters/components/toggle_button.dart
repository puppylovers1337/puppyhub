import 'package:flutter/material.dart';
import 'package:puppy_hub/models/match_filters.dart';

class ToggleButtonFilter extends StatefulWidget {
  final String text;
  final Function(Gender) addGender, removeGender;
  const ToggleButtonFilter({
    Key key,
    this.text,
    this.addGender,
    this.removeGender,
  }) : super(key: key);

  @override
  _ToggleButtonFilterState createState() => _ToggleButtonFilterState();
}

class _ToggleButtonFilterState extends State<ToggleButtonFilter> {
  bool _toggleValue = false;

  void toggleButton() {
    setState(() {
      _toggleValue = !_toggleValue;

      Gender gender =
          widget.text.toUpperCase() == 'MALE' ? Gender.male : Gender.female;

      if (_toggleValue) {
        widget.addGender(gender);
      } else {
        widget.removeGender(gender);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: toggleButton,
      child: Column(
        children: [
          Text(widget.text),
          SizedBox(height: 10),
          AnimatedContainer(
            duration: Duration(microseconds: 1000),
            height: 30,
            width: 70,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: _toggleValue
                  ? Theme.of(context).primaryColor
                  : Colors.grey.withOpacity(0.7),
            ),
            child: Stack(
              children: [
                AnimatedPositioned(
                  duration: Duration(microseconds: 1000),
                  curve: Curves.easeIn,
                  top: 3.0,
                  left: _toggleValue ? 40 : 0,
                  right: _toggleValue ? 0 : 40,
                  child: InkWell(
                    child: Icon(
                      Icons.circle,
                      color: Colors.white,
                      size: 25,
                      key: UniqueKey(),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
