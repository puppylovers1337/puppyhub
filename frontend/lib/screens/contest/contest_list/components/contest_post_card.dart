import 'package:flutter/material.dart';
import 'package:puppy_hub/components/rounded_button.dart';

class ContestPostCard extends StatelessWidget {
  final String title, prize, description, endDate;

  const ContestPostCard({
    Key key,
    this.title,
    this.description,
    this.endDate,
    this.prize,
  }) : super(key: key);

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Padding(
      padding: EdgeInsets.all(15),
      child: Container(
        decoration: new BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 4.0,
              spreadRadius: 0.0,
            )
          ],
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Padding(
          padding: EdgeInsets.only(top: 20, left: 25, right: 25, bottom: 5),
          child: Column(
            children: [
              Text(
                title.toUpperCase(),
                style: TextStyle(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: size.height * 0.03),
              Row(
                children: [
                  Text(
                    "PRIZE: ",
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(width: size.width * 0.03),
                  Expanded(
                    child: Text(
                      prize,
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: size.height * 0.01),
              Row(
                children: [
                  Text(
                    "ENDS: ",
                    style: TextStyle(
                      color: Colors.grey,
                    ),
                  ),
                  SizedBox(width: size.width * 0.03),
                  Expanded(
                    child: Text(
                      endDate,
                      style: TextStyle(
                        color: Colors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: size.height * 0.03),
              Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  description,
                  style: TextStyle(
                    fontSize: 16,
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.02),
              RoundedButton(
                text: "View contest",
                widthRatio: 0.80,
                press: () {},
              ),
            ],
          ),
        ),
      ),
    );
  }
}
