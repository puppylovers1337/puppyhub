import 'package:flutter/material.dart';

import 'contest_post_card.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ContestPostCard(
            title: "Dog of the month",
            description: "We reward the fluffiest dogs of the month",
            endDate: "14/06/2021",
            prize: "Our gratitude",
          ),
          ContestPostCard(
            title: "Dog of the month",
            description: "We reward the fluffiest dogs of the month",
            endDate: "14/06/2021",
            prize: "Our gratitude",
          ),
          ContestPostCard(
            title: "Dog of the month",
            description: "We reward the fluffiest dogs of the month",
            endDate: "14/06/2021",
            prize: "Our gratitude",
          ),
        ],
      ),
    );
  }
}
