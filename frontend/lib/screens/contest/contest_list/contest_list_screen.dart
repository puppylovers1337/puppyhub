import 'package:flutter/material.dart';
import 'package:puppy_hub/components/base_appbar.dart';
import 'package:puppy_hub/screens/chat/chat_list/components/avatar.dart';

import 'components/body.dart';

class ContestListScreen extends StatelessWidget {
  const ContestListScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'Contests',
        leadingWidget: Avatar(
          onPressed: () {},
        ),
      ),
      body: Body(),
    );
  }
}
