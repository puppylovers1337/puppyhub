import 'package:flutter/material.dart';
import 'package:puppy_hub/components/text_field_container.dart';

class RoundedInputField extends StatelessWidget {
  final String hintText;
  final IconData icon;
  final TextEditingController controller;
  final Function(String) validator;

  const RoundedInputField({
    Key key,
    this.hintText,
    this.icon,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: TextFormField(
        controller: controller,
        validator: validator,
        decoration: InputDecoration(
          icon: Icon(icon, color: Theme.of(context).primaryColor),
          hintText: hintText,
          border: InputBorder.none,
        ),
      ),
    );
  }
}
