import 'package:flutter/material.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function press;
  final Color backgroundColor, textColor;
  final double widthRatio;
  final EdgeInsetsGeometry margin;

  const RoundedButton({
    Key key,
    this.text,
    this.press,
    this.margin = const EdgeInsets.symmetric(vertical: 10),
    this.widthRatio = 1,
    this.backgroundColor,
    this.textColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    double width = size.width * this.widthRatio;

    return Container(
      margin: margin,
      width: width,
      child: TextButton(
        child: Text(
          text,
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
        onPressed: press,
        style: TextButton.styleFrom(
          padding: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
          primary: textColor,
          backgroundColor: backgroundColor ?? Theme.of(context).primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
      ),
    );
  }
}
