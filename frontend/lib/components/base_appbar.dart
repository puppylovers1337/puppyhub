import 'package:flutter/material.dart';
import 'package:puppy_hub/components/notification_button.dart';

class BaseAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final Widget leadingWidget;
  final Widget actionWidget;
  final bool hasArrowBack;

  const BaseAppBar({
    Key key,
    this.title,
    this.leadingWidget,
    this.actionWidget,
    this.hasArrowBack,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget _actionWidget = this.actionWidget ?? NotificationButton();
    Widget _leadingWidget = this.leadingWidget;
    bool _hasArrowBack = this.hasArrowBack ?? false;

    if (!_hasArrowBack && _leadingWidget != null) {
      _leadingWidget = Padding(
        padding: EdgeInsets.only(left: 20.0),
        child: _leadingWidget,
      );
    }

    return AppBar(
      automaticallyImplyLeading: _hasArrowBack,
      centerTitle: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(30),
        ),
      ),
      title: Text(this.title),
      leading: _leadingWidget,
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 20.0),
          child: _actionWidget,
        ),
      ],
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(kToolbarHeight);
}
