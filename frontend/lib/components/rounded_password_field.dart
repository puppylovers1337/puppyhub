import 'package:flutter/material.dart';
import 'package:puppy_hub/components/text_field_container.dart';

class RoundedPasswordField extends StatefulWidget {
  final TextEditingController controller;
  final Function(String) validator;

  const RoundedPasswordField({
    Key key,
    this.controller,
    this.validator,
  }) : super(key: key);

  @override
  _RoundedPasswordFieldState createState() => _RoundedPasswordFieldState();
}

class _RoundedPasswordFieldState extends State<RoundedPasswordField> {
  bool _obscureText;

  @override
  void initState() {
    super.initState();
    _obscureText = false;
  }

  @override
  Widget build(BuildContext context) {
    if (!_obscureText) {
      Future.delayed(Duration(milliseconds: 100), () {
        setState(() {
          _obscureText = true;
        });
      });
    }

    return TextFieldContainer(
      child: TextFormField(
        obscureText: _obscureText,
        controller: widget.controller,
        validator: widget.validator,
        decoration: InputDecoration(
          hintText: "Password",
          icon: Icon(Icons.lock, color: Theme.of(context).primaryColor),
          border: InputBorder.none,
          suffixIcon: GestureDetector(
            child:
                Icon(Icons.visibility, color: Theme.of(context).primaryColor),
            onTap: () {
              setState(() {
                _obscureText = false;
              });
            },
          ),
        ),
      ),
    );
  }
}
