import 'package:flutter/material.dart';

class NotificationButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.circle_notifications),
      tooltip: 'Notifications',
      onPressed: () {},
    );
  }
}
