import 'package:flutter/material.dart';
import 'package:puppy_hub/common/constants.dart';

import 'util/route_generator.dart';

void main() => runApp(TestApp());

class TestApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Test',
      theme: ThemeData(
        primaryColor: kPrimaryColor,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        scaffoldBackgroundColor: Colors.white,
      ),
      initialRoute: '/test',
      onGenerateRoute: RouteGenerator(false).generateRoute,
    );
  }
}
