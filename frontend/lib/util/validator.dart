class Validator {
  static final emailCheck = RegExp(
      r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

  static final numericCheck = RegExp(r'^-?(([0-9]*)|(([0-9]*)\.([0-9]*)))$');

  static final Function(String) username = (value) {
    if (value.isEmpty) {
      return "Enter username.";
    }

    if (value.length < 4) {
      return "Need minimum 4 characters.";
    }

    return null;
  };

  static final Function(String) dogName = (value) {
    if (value.isEmpty) {
      return "Enter dog name.";
    }

    if (value.length < 3) {
      return "Need minimum 3 characters.";
    }

    return null;
  };

  static final Function(String) email = (value) {
    if (value.isEmpty) {
      return "Enter email.";
    }

    if (!emailCheck.hasMatch(value)) {
      return "Invalid email";
    }

    return null;
  };

  static final Function(String) password = (value) {
    if (value.isEmpty) {
      return "Enter password.";
    }
    return null;
  };

  static final Function(String) breed = (value) {
    if (value == null) {
      return "Breed is mandatory.";
    }
    return null;
  };

  static String mandatory(String text, String value) {
    if (value.isEmpty) {
      return "$text is mandatory.";
    }
    return null;
  }

  static final Function(String) optionalPrice = (value) {
    if (value != null && !numericCheck.hasMatch(value)) {
      return "Price is invalid number.";
    }

    return null;
  };
}
