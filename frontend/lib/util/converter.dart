import 'package:flutter/foundation.dart';
import 'package:intl/intl.dart';
import 'package:puppy_hub/models/dog.dart';

class Converter {
  static String toChatListFormat(DateTime date) {
    return DateFormat('dd MMM - HH:mm').format(date.toLocal());
  }

  static String toChatMessageFormat(DateTime date) {
    return DateFormat('HH:mm').format(date.toLocal());
  }

  static String toMarketPostFormat(DateTime date) {
    return DateFormat('dd MMM yyyy HH:mm').format(date.toLocal());
  }

  // TODO: a way to convert string to enum generic
  static Gender stringToGender(String str) {
    return Gender.values.firstWhere((e) => describeEnum(e) == str);
  }
}
