import "package:flutter/material.dart";
import 'package:puppy_hub/screens/auth/login/login_screen.dart';
import 'package:puppy_hub/screens/auth/signup/signup_screen.dart';
import 'package:puppy_hub/screens/chat/chat_user/chat_user_screen.dart';
import 'package:puppy_hub/screens/main_screen/main_screen.dart';
import 'package:puppy_hub/screens/market/change_market_post/change_market_post_screen.dart';
import 'package:puppy_hub/screens/market/market_view/market_view_screen.dart';
import 'package:puppy_hub/screens/match/match/match_screen.dart';
import 'package:puppy_hub/screens/match/match_filters/match_filters_screen.dart';
import 'package:puppy_hub/screens/user/account/account_screen.dart';
import 'package:puppy_hub/screens/user/change_dog_profile/change_dog_profile_screen.dart';
import 'package:puppy_hub/screens/user/dog_profile/dog_profile_screen.dart';
import 'package:puppy_hub/screens/user/edit_account/edit_account_screen.dart';
import 'package:puppy_hub/screens/welcome/welcome_screen.dart';

class RouteGenerator {
  bool _isLoggedIn;

  RouteGenerator(bool isLoggedIn) {
    _isLoggedIn = isLoggedIn;
  }

  Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    if (!_isLoggedIn) {
      switch (settings.name) {
        case '/':
          return MaterialPageRoute(builder: (_) => WelcomeScreen());
        case '/login':
          return MaterialPageRoute(builder: (_) => LoginScreen());
        case '/register':
          return MaterialPageRoute(builder: (_) => SignUpScreen());
        default:
          return _errorRoute();
      }
    }

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => MainScreen());

      case '/dog/edit':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('dogId')) {
            return MaterialPageRoute(
                builder: (_) => ChangeDogProfileScreen(dogId: argMap['dogId']));
          }
        }
        return _errorRoute();

      case '/dog/new':
        return MaterialPageRoute(builder: (_) => ChangeDogProfileScreen());

      case '/dog':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('dogId')) {
            return MaterialPageRoute(
                builder: (_) => DogProfile(dogId: argMap['dogId']));
          }
        }
        return _errorRoute();

      case '/account/edit':
        return MaterialPageRoute(builder: (_) => AccountEditScreen());

      case '/account':
        return MaterialPageRoute(builder: (_) => AccountScreen());

      case '/chat/user':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('roomId')) {
            return MaterialPageRoute(
                builder: (_) => ChatUserScreen(roomId: argMap['roomId']));
          }
        }
        return _errorRoute();

      case '/match':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('dogId') && argMap.containsKey('filters')) {
            return MaterialPageRoute(
                builder: (_) => MatchScreen(
                    dogId: argMap['dogId'], filters: argMap['filters']));
          }
        }
        return _errorRoute();

      case '/match/filters':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('dogId')) {
            return MaterialPageRoute(
                builder: (_) => MatchFiltersScreen(dogId: argMap['dogId']));
          }
        }
        return _errorRoute();

      case '/market/post':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('postId')) {
            return MaterialPageRoute(
                builder: (_) => MarketViewScreen(postId: argMap['postId']));
          }
        }
        return _errorRoute();

      case '/market/post/new':
        return MaterialPageRoute(builder: (_) => ChangeMarketPostScreen());

      case '/market/post/edit':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('postId')) {
            return MaterialPageRoute(
                builder: (_) =>
                    ChangeMarketPostScreen(postId: argMap['postId']));
          }
        }
        return _errorRoute();
      case '/market/post':
        if (args != null) {
          var argMap = Map<String, dynamic>.from(args);
          if (argMap.containsKey('postId')) {
            return MaterialPageRoute(
                builder: (_) => MarketViewScreen(postId: argMap['postId']));
          }
        }
        return _errorRoute();
      default:
        return _errorRoute();
    }
  }

  Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('404'),
        ),
        body: Center(
          child: Text('Page not found.'),
        ),
      );
    });
  }
}
