import 'dart:typed_data';

import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mime/mime.dart';
import 'package:http_parser/http_parser.dart';

Future<Uint8List> readImageAsBytes() async {
  PickedFile _pickedImage =
      await ImagePicker().getImage(source: ImageSource.gallery);

  if (_pickedImage != null) {
    return await _pickedImage.readAsBytes();
  }
  return null;
}

MediaType mimeTypeFromBytes(Uint8List bytes) {
  var mimeType = lookupMimeType('fake_path',
      headerBytes: bytes.sublist(0, defaultMagicNumbersMaxLength));
  if (mimeType == null) return null;

  var mimeTypeParts = mimeType.split('/');
  return MediaType(mimeTypeParts[0], mimeTypeParts[1]);
}

FormData getFormData(Uint8List bytes) {
  return FormData.fromMap({
    'image': MultipartFile.fromBytes(
      bytes,
      filename: 'fake_name',
      contentType: mimeTypeFromBytes(bytes),
    )
  });
}
